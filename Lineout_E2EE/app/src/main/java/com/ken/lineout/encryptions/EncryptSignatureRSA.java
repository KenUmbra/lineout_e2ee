package com.ken.lineout.encryptions;

import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;

public class EncryptSignatureRSA extends EncryptRSA {
    private final byte[] privateKey;

    /**
     * Constructor for the RSA Signature encoder class using a RSAPair object.
     * @param key The private key used for decryption (byte[]).
     */
    public EncryptSignatureRSA(byte[] key)
    {
        this.privateKey = key;
    }


    /**
     * This function encrypts a numerical value using RSA encryption.
     * @param key The public RSA key used for encryption (byte[]).
     * @param value The numerical value to be encrypted by the function (byte[]).
     * @return The encrypted value (byte[]).
     * @throws NoSuchAlgorithmException if hashing algorithm does not exist.
     */
    @Override
    public byte[] encrypt(byte[] key, byte[] value) throws NoSuchAlgorithmException
    {
        byte[] encryptedValue;
        BigInteger paddedValue;
        BigInteger bigPrivate = new BigInteger(privateKey);
        BigInteger bigKey = new BigInteger(1, key);

        do {
            paddedValue = padOAEP(value);
        } while(paddedValue.signum() == -1);


        encryptedValue = (paddedValue.modPow(bigPrivate, bigKey)).toByteArray();

        return encryptedValue;
    }

    /**
     * This function generates padding for the plain data using the OAEP padding scheme for improved security.
     * @param plainValue The value to pad (byte[]).
     * @return The padded value (BigInteger).
     * @throws NoSuchAlgorithmException if hashing algorithm does not exist.
     */
    @Override
    protected BigInteger padOAEP(byte[] plainValue) throws NoSuchAlgorithmException
    {
        return super.padOAEP(plainValue);
    }
}
