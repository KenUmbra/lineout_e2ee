package com.ken.lineout.packet_handling;

import android.util.Log;

import androidx.annotation.NonNull;

import com.ken.lineout.encryptions.CryptoManager;

import org.json.JSONArray;
import org.json.JSONObject;

import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.util.ArrayList;

public class PacketDeserializer {
    private static final String TAG = "PacketDeserializer";

    public static final byte ERROR_OPCODE = -1;
    public static final byte SIGNUP_RESULT_OPCODE = 2;
    public static final byte LOGIN_RESULT_OPCODE = 4;
    public static final byte ADD_FRIEND_RESULT_OPCODE = 12;
    public static final byte ADD_MESSAGE_RESULT_OPCODE = 14;
    public static final byte GET_MESSAGES_RESULT_OPCODE = 16;

    public static final int RESULT_OK = 1;

    private CryptoManager cryptoManager;
    private byte[] userPubKeyRSA;

    /**
     * This function crates an instance of the PacketSerializer class.
     * @param cryptoManager The user's CryptoManager (CryptoManager).
     * @param userPubKeyRSA The user's public RSA key (byte[]).
     */
    public PacketDeserializer(CryptoManager cryptoManager, byte[] userPubKeyRSA)
    {
        this.cryptoManager = cryptoManager;
        this.userPubKeyRSA = userPubKeyRSA;
    }

    /**
     * This function deserializes any action response packets.
     * @param data The encrypted data (byte[]).
     * @param keyAES The AES key to be used during decryption (byte[]).
     * @return True - Action Successful, False - Action Failed (boolean).
     * @throws Exception if decryption fails.
     */
    public boolean deserializeActionPacket(byte[] data, byte[] keyAES) throws Exception
    {
        JSONObject jsonFields;
        byte[] decryptedData;

        decryptedData = cryptoManager.decryptData(CryptoManager.AES_MODE, data, keyAES);
        jsonFields = new JSONObject(new String(decryptedData));

        return jsonFields.getInt("result") == RESULT_OK;
    }

    /**
     * This function deserializes any error response packets.
     * @param data The encrypted data (byte[]).
     * @return The reason for the error (String).
     * @throws Exception if decryption fails.
     */
    public String deserializeErrorPacket(byte[] data) throws Exception
    {
        JSONObject jsonFields;
        jsonFields = new JSONObject(new String(data));

        return jsonFields.getString("reason_for_error");
    }

    /**
     * This function deserializes get RSA key response packets.
     * @param data The encrypted data (byte[]).
     * @param keyAES The AES key to be used during decryption (byte[]).
     * @return The requested RSA public key (byte[]).
     * @throws Exception if decryption fails.
     */
    public byte[] deserializeGetKeyPacket(byte[] data, byte[] keyAES) throws Exception
    {
        JSONArray keyAsJson;
        ArrayList<Byte> keyAsObject = new ArrayList<>();
        JSONObject jsonFields;
        byte[] decryptedData, keyAsPrimitive;
        int i;

        decryptedData = cryptoManager.decryptData(CryptoManager.AES_MODE, data, keyAES);
        jsonFields = new JSONObject(new String(decryptedData));
        keyAsJson = jsonFields.getJSONArray("key");
        for(i = 0; i < keyAsJson.length(); i++)
        {
            keyAsObject.add(((Integer) keyAsJson.getInt(i)).byteValue());
        }

        keyAsPrimitive = new byte[keyAsObject.size()];
        for (i = 0; i < keyAsObject.size(); i++) {
            keyAsPrimitive[i] = keyAsObject.get(i);
        }

        return keyAsPrimitive;
    }

    /**
     * This function deserializes get messages response packets.
     * @param data The encrypted data (byte[]).
     * @param keyAES The AES key to be used during decryption (byte[]).
     * @return The list of message packets intended for the user (ArrayList<JSONObject>).
     * @throws Exception if decryption fails.
     */
    public ArrayList<JSONObject> deserializeGetMessagesPacket(byte[] data, byte[] keyAES) throws Exception
    {
        ArrayList<ArrayList<Byte>> encryptedMessages = new ArrayList<>();
        ArrayList<JSONObject> messages = new ArrayList<>();
        ArrayList<Byte> currentEncryptedMessage;
        JSONArray jsonEncryptedMessages;
        JSONObject jsonFields;
        byte[] decryptedData, encryptedMsg;
        int i, j;

        decryptedData = cryptoManager.decryptData(CryptoManager.AES_MODE, data, keyAES);
        jsonFields = new JSONObject(new String(decryptedData));
        jsonEncryptedMessages = jsonFields.getJSONArray("messages");
        if(jsonEncryptedMessages.length() != 0)
        {
            for (i = 0; i < jsonEncryptedMessages.length(); i++) {
                currentEncryptedMessage = new ArrayList<>();

                for (j = 0; j < jsonEncryptedMessages.getJSONArray(i).length(); j++) {
                    currentEncryptedMessage.add(((Integer) jsonEncryptedMessages.getJSONArray(i).get(j)).byteValue());
                }

                encryptedMessages.add(currentEncryptedMessage);
            }

            for (ArrayList<Byte> encryptedMsgObject: encryptedMessages) {
                encryptedMsg = new byte[encryptedMsgObject.size()];

                for (i = 0; i < encryptedMsgObject.size(); i++) {
                    encryptedMsg[i] = encryptedMsgObject.get(i);
                }

                messages.add(this.deserializeEncryptedData(encryptedMsg));
            }
        }

        return messages;
    }

    /**
     * This function deserializes encrypted data into a JSON object.
     * @param data The encrypted data (byte[]).
     * @return The serialized JSON object (JSONObject).
     * @throws Exception if decryption fails.
     */
    private JSONObject deserializeEncryptedData(@NonNull byte[] data) throws Exception
    {
        JSONObject decryptedMsg;
        ByteBuffer sizeBuffer;
        byte[] sizeInBytes, keyAES, encryptedData, decryptedData;
        int nextFieldLen, index = 0, keySize;

        nextFieldLen = data[index++];
        keyAES = new byte[(nextFieldLen & 0xFF)];
        System.arraycopy(data, index, keyAES, 0, keyAES.length);
        index += keyAES.length;

        sizeInBytes = new byte[]{data[index], data[index + 1]};
        index += sizeInBytes.length;
        //sizeBuffer = ByteBuffer.wrap(sizeInBytes);
        nextFieldLen = new BigInteger(1, sizeInBytes).intValue();
        encryptedData = new byte[nextFieldLen];
        System.arraycopy(data, index, encryptedData, 0, encryptedData.length);

        keyAES = cryptoManager.decryptData(CryptoManager.RSA_MODE, keyAES, this.userPubKeyRSA);
        decryptedData = cryptoManager.decryptData(CryptoManager.AES_MODE, encryptedData, keyAES);
        decryptedMsg = new JSONObject(new String(decryptedData));

        return decryptedMsg;
    }

    /**
     * This function returns the deserializer's crypto manager.
     * @return The deserializer's crypto manager (CryptoManager).
     */
    public CryptoManager getCryptoManager() {
        return cryptoManager;
    }

    /**
     * This function returns the user's public RSA key.
     * @return The user's public RSA key (byte[]).
     */
    public byte[] getUserPubKeyRSA() {
        return userPubKeyRSA;
    }
}
