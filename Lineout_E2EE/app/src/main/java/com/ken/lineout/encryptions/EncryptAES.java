package com.ken.lineout.encryptions;

import java.io.Serializable;

import com.ken.lineout.encryptions.lineout_key_generators.*;

public class EncryptAES implements IEncrypt, Serializable
{
    private final UtilityAES UTILITY_AES = new UtilityAES();

    /**
     * This function gets the output length by taking the input length and adding the necessary padding.
     * @param len The original length of the input (int).
     * @return The output length (int).
     */
    private int getOutputLength(int len)
    {
        int lenWithPadding = len / KeyGenerator.AES_KEY_SIZE_BYTES;
        if(len % KeyGenerator.AES_KEY_SIZE_BYTES != 0)
        {
            lenWithPadding++;
        }

        lenWithPadding *= KeyGenerator.AES_KEY_SIZE_BYTES;

        return lenWithPadding;
    }

    /**
     * This functions creates a padded input array.
     * @param input The original input (byte[]).
     * @return The input after padding (byte[]).
     */
    private byte[] nullPadding(byte[] input)
    {
        int i;
        byte[] output = new byte[getOutputLength(input.length)];
        System.arraycopy(input, 0, output, 0, input.length);

        for(i = input.length; i < output.length; i++)
        {
            output[i] = 0x00;
        }

        return output;
    }

    /**
     * This function performs S-BOX transformation to the state block.
     * @param state The state block (byte[][]).
     */
    private void subBytes(byte[][] state)
    {
        int i;
        
        for(i = 0; i < state.length; i++)
        {
            state[i] = UTILITY_AES.subWord(state[i]);
        }

    }

    /**
     * This function shifts the rows in the state based on Rijndael ShiftRows.
     * @param state The state block (byte[][]).
     */
    private void shiftRows(byte[][] state)
    {
        int i, j;
        for(i = 1; i <  UtilityAES.WORD_IN_BYTES; i++)
        {
            for(j = 0; j < i; j++)
            {
                state[i] = UTILITY_AES.rotateWord(state[i]);
            }
        }
    }

    /**
     * This function mixes a single column according to Rijndael MixColumns.
     * @param column The column to be mixed (byte[]).
     */
    private void mixSingleColumn(byte[] column)
    {
        byte[] colCopy = new byte[UtilityAES.WORD_IN_BYTES];
        int i;

        for(i = 0; i < UtilityAES.WORD_IN_BYTES; i++)
        {
            colCopy[i] = column[i];
        }

        column[0] = (byte) (UTILITY_AES.mulBytes((byte) 0x02, colCopy[0]) ^ UTILITY_AES.mulBytes((byte) 0x03, colCopy[1]) ^ colCopy[2] ^ colCopy[3]);
        column[1] = (byte) (colCopy[0] ^ UTILITY_AES.mulBytes((byte) 0x02, colCopy[1]) ^ UTILITY_AES.mulBytes((byte) 0x03, colCopy[2]) ^ colCopy[3]);
        column[2] = (byte) (colCopy[0] ^ colCopy[1] ^ UTILITY_AES.mulBytes((byte) 0x02, colCopy[2]) ^ UTILITY_AES.mulBytes((byte) 0x03, colCopy[3]));
        column[3] = (byte) (UTILITY_AES.mulBytes((byte) 0x03, colCopy[0]) ^ colCopy[1] ^ colCopy[2] ^ UTILITY_AES.mulBytes((byte) 0x02, colCopy[3]));

    }

    /**
     * This function mixes the columns in the state block.
     * @param state The state block (byte[][]).
     */
    private void mixColumns(byte[][] state)
    {
        byte[] temp = new byte[UtilityAES.WORD_IN_BYTES];
        int i, j;

        for(i = 0; i < UtilityAES.WORD_IN_BYTES; i++)
        {
            for(j = 0; j < UtilityAES.WORD_IN_BYTES; j++)
            {
                temp[j] = state[j][i];
            }

            mixSingleColumn(temp);

            for(j = 0; j < UtilityAES.WORD_IN_BYTES; j++)
            {
                state[j][i] = temp[j];
            }
        }

    }

    /**
     * This function encodes a block of information using AES.
     * @param input The input to be encrypted by the function (byte[]).
     * @param roundKeys The round keys (byte[]).
     * @return The encrypted output (byte[]).
     */
    private byte[] encryptBlock(byte[] input, byte[] roundKeys)
    {
        byte[][] state = new byte[UtilityAES.WORD_IN_BYTES][UtilityAES.WORD_IN_BYTES];
        byte[] output = new byte[KeyGenerator.AES_KEY_SIZE_BYTES];
        byte[] currentKey = new byte[KeyGenerator.AES_KEY_SIZE_BYTES];
        int i, j, round;

        for(i = 0; i < state.length; i++)
        {
            for(j = 0; j < state[0].length; j++)
            {
                state[i][j] = input[i + UtilityAES.WORD_IN_BYTES * j];
            }
        }

        System.arraycopy(roundKeys, 0, currentKey, 0, KeyGenerator.AES_KEY_SIZE_BYTES);
        state = UTILITY_AES.addRoundKey(state, currentKey);

        for(round = 1; round <= UtilityAES.AMOUNT_OF_ROUNDS - 1; round++)
        {
            subBytes(state);
            shiftRows(state);
            mixColumns(state);

            System.arraycopy(roundKeys, round * KeyGenerator.AES_KEY_SIZE_BYTES, currentKey, 0, KeyGenerator.AES_KEY_SIZE_BYTES);
            state = UTILITY_AES.addRoundKey(state, currentKey);
        }

        subBytes(state);
        shiftRows(state);

        System.arraycopy(roundKeys, UtilityAES.AMOUNT_OF_ROUNDS * KeyGenerator.AES_KEY_SIZE_BYTES, currentKey, 0, KeyGenerator.AES_KEY_SIZE_BYTES);
        state = UTILITY_AES.addRoundKey(state, currentKey);
        
        for(i = 0; i < UtilityAES.WORD_IN_BYTES; i++)
        {
            for(j = 0; j < UtilityAES.WORD_IN_BYTES; j++)
            {
                output[i + UtilityAES.WORD_IN_BYTES * j] = state[i][j];
            }
        }

        return output;
    }

    /**
     * This function encrypts input using AES Encryption.
     * @param key The AES key used used for encryption (byte[]).
     * @param input The input to be encrypted by the function (byte[]).
     * @return The encrypted input (byte[]).
     */
    public byte[] encrypt(byte[] key, byte[] input)
    {
        byte[] alignInput = nullPadding(input);
        byte[] temp = new byte[KeyGenerator.AES_KEY_SIZE_BYTES];
        byte[] output = new byte[alignInput.length];
        byte[] roundKeys = UTILITY_AES.keyExpansion(key);
        int i;

        for(i = 0; i < output.length; i += KeyGenerator.AES_KEY_SIZE_BYTES)
        {
            System.arraycopy(alignInput, i, temp, 0, KeyGenerator.AES_KEY_SIZE_BYTES);
            System.arraycopy(encryptBlock(temp, roundKeys), 0, output, i, KeyGenerator.AES_KEY_SIZE_BYTES);
        }

        return output;
    }
}