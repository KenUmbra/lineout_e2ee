package com.ken.lineout.encryptions;

public interface IDecrypt
{
    default byte[] decrypt(byte[] key, byte[] value) throws Exception {
        return new byte[0];
    }
}
