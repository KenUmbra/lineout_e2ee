package com.ken.lineout;

import android.content.Intent;
import android.content.IntentFilter;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.ken.lineout.database.DatabaseWrapperSQL;
import com.ken.lineout.encryptions.CryptoManager;
import com.ken.lineout.encryptions.lineout_key_generators.KeyGenerator;
import com.ken.lineout.encryptions.lineout_key_generators.RSAPair;
import com.ken.lineout.networking.LineoutApp;
import com.ken.lineout.networking.LineoutBroadcastReceiver;
import com.ken.lineout.networking.LineoutManager;
import com.ken.lineout.networking.LineoutWriter;
import com.ken.lineout.packet_handling.PacketDeserializer;
import com.ken.lineout.packet_handling.PacketSerializer;

import java.io.IOException;
import java.math.BigInteger;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;

public class SignupActivity extends AppCompatActivity {
    public static final String CRYPT_NAME = "CryptoManager";
    public static final String KEY_NAME = "UserPublicKey";

    public static final int USERNAME_MAX_LENGTH = 32;

    private RSAPair keyPair;
    private CryptoManager cryptoManager;
    private PacketSerializer serializer;
    private PacketDeserializer deserializer;

    private DatabaseWrapperSQL wrapperSQL;

    private LineoutApp lineoutApp;

    private IntentFilter intentFilter = new IntentFilter();
    private LineoutBroadcastReceiver broadcastReceiver = null;
    private LineoutManager lineoutManager = null;
    private LineoutWriter lineoutWriter;
    private Thread writerThread;
    private Socket connectionSocket;

    private EditText usernameET, passwordET;
    private Button signupBtn, moveToLoginBtn;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        Intent retIntent = getIntent();
        SQLiteDatabase db = openOrCreateDatabase(DatabaseWrapperSQL.DB_FILE_NAME, MODE_PRIVATE, null);
        boolean firstTime = retIntent.getBooleanExtra("FirstTime", true);
        lineoutApp = (LineoutApp) getApplication();
        wrapperSQL = new DatabaseWrapperSQL(db);

        intentFilter.addAction(LineoutBroadcastReceiver.ERROR_ACTION);
        intentFilter.addAction(LineoutBroadcastReceiver.SIGNUP_ACTION);

        if(firstTime) {
            if(wrapperSQL.countFriends() != 0)
            {
                HashMap<String, Object> userFields = wrapperSQL.getUserData();
                ArrayList<Byte> keyObject;
                byte[] keyPrimitive;
                int i;

                keyPair = new RSAPair();

                keyObject = (ArrayList<Byte>) userFields.get("PUBLIC_KEY");
                keyPrimitive = new byte[keyObject.size()];
                for (i = 0; i < keyObject.size(); i++) {
                    keyPrimitive[i] = keyObject.get(i);
                }
                keyPair.publicKey = new BigInteger(keyPrimitive);

                keyObject = (ArrayList<Byte>) userFields.get("PRIVATE_KEY");
                keyPrimitive = new byte[keyObject.size()];
                for (i = 0; i < keyObject.size(); i++) {
                    keyPrimitive[i] = keyObject.get(i);
                }
                keyPair.privateKey = new BigInteger(keyPrimitive);

            }
            else
            {
                keyPair = KeyGenerator.generateKeysRSA();
            }

            cryptoManager = new CryptoManager(keyPair.privateKey.toByteArray());

            serializer = new PacketSerializer(cryptoManager);
            deserializer = new PacketDeserializer(cryptoManager, keyPair.publicKey.toByteArray());
            lineoutManager = new LineoutManager(deserializer, getApplicationContext());
            lineoutManager.start();
            while(lineoutManager.getSocket() == null){}
            lineoutApp.setMasterSocket(lineoutManager.getSocket());
        }
        else
        {
            keyPair = new RSAPair();
            keyPair.publicKey = new BigInteger(retIntent.getByteArrayExtra(KEY_NAME));
            cryptoManager = (CryptoManager) retIntent.getSerializableExtra(CRYPT_NAME);
            serializer = new PacketSerializer(cryptoManager);
            deserializer = new PacketDeserializer(cryptoManager, keyPair.publicKey.toByteArray());
        }

        connectionSocket  = lineoutApp.getMasterSocket();

        usernameET = findViewById(R.id.signup_username_et);
        passwordET = findViewById(R.id.signup_password_et);
        signupBtn = findViewById(R.id.signup_button);
        moveToLoginBtn = findViewById(R.id.move_to_login_page_button);

        signupBtn.setOnClickListener(this::onClickSignup);

        moveToLoginBtn.setOnClickListener(view -> {
            Intent intent = new Intent(SignupActivity.this, LoginActivity.class);
            intent.putExtra(CRYPT_NAME, cryptoManager);
            intent.putExtra(KEY_NAME, keyPair.publicKey.toByteArray());
            startActivity(intent);
        });
    }

    /**
     * This function handles clicks on the signup button.
     * @param view The current view (View).
     */
    public void onClickSignup(View view)
    {
        PacketSerializer.Packet signupPacket = new PacketSerializer.Packet();
        ArrayList<Byte> publicKeyRSA = new ArrayList<>();

        if(wrapperSQL.countFriends() == 0) {
            if (!usernameET.getText().toString().equals("") && !passwordET.getText().toString().equals("") && USERNAME_MAX_LENGTH > usernameET.getText().toString().length()) {
                signupPacket.opcode = PacketSerializer.SIGNUP_OPCODE;

                signupPacket.fields = new HashMap<>();
                signupPacket.fields.put("username", usernameET.getText().toString());
                signupPacket.fields.put("password", passwordET.getText().toString());
                for (byte b : keyPair.publicKey.toByteArray()) {
                    publicKeyRSA.add(b);
                }
                signupPacket.fields.put("pub_key", publicKeyRSA);

                broadcastReceiver = new LineoutBroadcastReceiver(wrapperSQL, usernameET.getText().toString(), keyPair.publicKey.toByteArray(), keyPair.privateKey.toByteArray());
                lineoutApp.setLineoutBroadcastReceiver(broadcastReceiver, intentFilter);

                if(lineoutManager != null) {
                    lineoutWriter = new LineoutWriter(lineoutManager.getOutputStream(), serializer, signupPacket);
                } else {
                    try {
                        lineoutWriter = new LineoutWriter(connectionSocket.getOutputStream(), serializer, signupPacket);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                writerThread = new Thread(lineoutWriter);
                writerThread.start();
            }
        }
        else
        {
            Toast.makeText(getApplicationContext(), "Lineout Account Linked Already", Toast.LENGTH_SHORT).show();
        }
    }
}
