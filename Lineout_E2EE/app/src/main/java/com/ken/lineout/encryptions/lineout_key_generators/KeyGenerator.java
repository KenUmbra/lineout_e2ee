package com.ken.lineout.encryptions.lineout_key_generators;

import static java.math.BigInteger.*;
import java.math.BigInteger;
import java.util.Random;

public class KeyGenerator
{
    public static final int AES_KEY_SIZE_BYTES = 16;
    public static final int RSA_KEY_SIZE_BITS = 1024;
    public static final BigInteger PUBLIC_KEY_MODIFIER = BigInteger.valueOf(4079);

    /**
     * This function generates a 128-bit AES Key.
     * @return 128-bit AES key (byte[]).
     */
    public static byte[] generateKeyAES()
    {
        int i;
        int max_value = 255;
        byte[] secretKey = new byte[AES_KEY_SIZE_BYTES];
        BigInteger bigKey;
        byte x = 0;
        
        do
        {
            for (i = 0; i < secretKey.length; i++)
            {
                x = (byte)(Math.random() * max_value);
                secretKey[i] = x;
            }
            bigKey = new BigInteger(secretKey);
        } while(bigKey.compareTo(ZERO) < 0);

        return secretKey;
    }

    /**
     * This function generates a pair of 1024-bit RSA keys.
     * @return A pair of 1024-bit RSA keys (RSAPair).
     */
    public static RSAPair generateKeysRSA()
    {
        RSAPair secretKeys = new RSAPair();
        Random rnd = new Random();
        BigInteger firstPrime;
        BigInteger secondPrime;
        BigInteger modulus;

        do {
           firstPrime = probablePrime(RSA_KEY_SIZE_BITS/2, rnd);
           secondPrime = firstPrime.nextProbablePrime();
           modulus = firstPrime.subtract(ONE).multiply(secondPrime.subtract(ONE));
        } while ((modulus.gcd(PUBLIC_KEY_MODIFIER).intValue() != ONE.intValue()) || (firstPrime.multiply(secondPrime).bitLength() != RSA_KEY_SIZE_BITS));

        secretKeys.publicKey = firstPrime.multiply(secondPrime);

        secretKeys.privateKey = PUBLIC_KEY_MODIFIER.modInverse(modulus);

        return secretKeys;
    }
}