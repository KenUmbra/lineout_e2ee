package com.ken.lineout.encryptions.lineout_key_generators;

import java.math.BigInteger;

public class RSAPair 
{
    public BigInteger publicKey;
    public BigInteger privateKey;
}
