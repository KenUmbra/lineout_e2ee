package com.ken.lineout.networking;

import android.util.Base64;
import android.util.Log;

import com.ken.lineout.packet_handling.PacketSerializer;

import java.io.OutputStream;
import java.io.Serializable;

public class LineoutWriter implements Runnable, Serializable {
    private static final String B64_SERVER_KEY_RSA = "h1Um9vM3wOw+w/m3P+nADPFLc86erVdToZ7WZRe7uT66riYRIS0QdInW9ng1s9NjP/FOWxy5Jm/p5nPolo4Fj2/b/+qdgQZBaHbxp0hv1Qoy/DhJnheCBnDXYvJFgMTyz6pskcU5CBz8Wbr6u3Q8pIuVYab7IXVT+86e+8Kmf9k=";
    private static final String TAG = "LineoutWriter";

    private OutputStream outputStream;
    private PacketSerializer serializer;
    private PacketSerializer.Packet packet;
    private String destUsername;
    private byte[] destKeyRSA;


    /**
     * This function creates a new instance of a LineoutWriter object.
     * @param outputStream The output stream of the socket that is connected to the Lineout server (OutputStream).
     * @param packet The packet to be sent to the server (PacketSerializer.Packet).
     */
    public LineoutWriter(OutputStream outputStream, PacketSerializer.Packet packet)
    {
        this.outputStream = outputStream;
        this.packet = packet;
    }

    /**
     * This function creates a new instance of a LineoutWriter object.
     * @param outputStream The output stream of the socket that is connected to the Lineout server (OutputStream).
     * @param serializer The packet serializer to be used (PacketSerializer).
     * @param packet The packet to be sent to the server (PacketSerializer.Packet).
     */
    public LineoutWriter(OutputStream outputStream, PacketSerializer serializer, PacketSerializer.Packet packet)
    {
        this.outputStream = outputStream;
        this.serializer = serializer;
        this.packet = packet;
    }

    /**
     * This function creates a new instance of a LineoutWriter object.
     * @param outputStream The output stream of the socket that is connected to the Lineout server (OutputStream).
     * @param serializer The packet serializer to be used (PacketSerializer).
     * @param packet The packet to be sent to the server (PacketSerializer.Packet).
     * @param destUsername The username of the friend the packet is intended for (String).
     * @param destKeyRSA The friend's public RSA key (byte[]).
     */
    public LineoutWriter(OutputStream outputStream, PacketSerializer serializer, PacketSerializer.Packet packet, String destUsername, byte[] destKeyRSA)
    {
        this.outputStream = outputStream;
        this.serializer = serializer;
        this.packet = packet;
        this.destUsername = destUsername;
        this.destKeyRSA = destKeyRSA;
    }

    /**
     * This functions sends the packet to the Lineout server.
     */
    @Override
    public void run()
    {
        byte[] serverKeyRSA = Base64.decode(B64_SERVER_KEY_RSA, Base64.DEFAULT);

        try {
            switch (packet.opcode) {
                case PacketSerializer.SIGNUP_OPCODE:
                case PacketSerializer.LOGIN_OPCODE:
                case PacketSerializer.ADD_FRIEND_OPCODE:
                    outputStream.write(this.serializer.serializePacket(this.packet, serverKeyRSA));
                    break;

                case PacketSerializer.GET_MESSAGES_OPCODE:
                    outputStream.write(packet.opcode);
                    break;

                case PacketSerializer.ADD_MESSAGE_OPCODE:
                    outputStream.write(this.serializer.serializePacket(this.packet, destKeyRSA, this.destUsername, serverKeyRSA));
                    break;

                default:
                    Log.d(TAG, "run: Unknown Opcode");
                    break;
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            Log.d(TAG, "run: Couldn't Serialize Packet");
        }

    }
}
