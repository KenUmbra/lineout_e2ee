package com.ken.lineout;

import android.content.Intent;
import android.content.IntentFilter;
import android.database.DataSetObserver;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Handler;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.ken.lineout.database.DatabaseWrapperSQL;
import com.ken.lineout.encryptions.CryptoManager;
import com.ken.lineout.encryptions.HashMaker;
import com.ken.lineout.gui.ChatArrayAdapter;
import com.ken.lineout.gui.ChatMessage;
import com.ken.lineout.networking.LineoutApp;
import com.ken.lineout.networking.LineoutBroadcastReceiver;
import com.ken.lineout.networking.LineoutWriter;
import com.ken.lineout.packet_handling.PacketSerializer;

import java.io.IOException;
import java.net.Socket;
import java.security.MessageDigest;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.Locale;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;

public class ChatActivity extends AppCompatActivity {

    private CryptoManager cryptoManager;
    private PacketSerializer serializer;

    private DatabaseWrapperSQL wrapperSQL;

    private LineoutApp lineoutApp;
    private TextToSpeech ttsManager;

    private IntentFilter intentFilter = new IntentFilter();
    private LineoutBroadcastReceiver broadcastReceiver = null;
    private LineoutWriter lineoutWriter;
    private Thread writerThread;
    private Socket connectionSocket;

    private ArrayList<HashMap<String, Object>> messages = new ArrayList<>();
    private Handler refreshHandler = new Handler();
    private ChatArrayAdapter chatArrayAdapter;
    private TimerTask refreshTask;
    private Timer refreshTimer;
    private String chatName;

    private byte[] publicKey;
    private boolean timerWorking = false;
    private boolean isTTSEnabled = true;
    private int chatId;

    private TextView friendNameTV;
    private EditText messageET;
    private ListView msgList;
    private Button sendBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        lineoutApp = (LineoutApp) getApplication();
        Intent retIntent = getIntent();
        SQLiteDatabase db = openOrCreateDatabase(DatabaseWrapperSQL.DB_FILE_NAME, MODE_PRIVATE, null);
        ArrayList<HashMap<String, Object>> chats;
        wrapperSQL = new DatabaseWrapperSQL(db);

        intentFilter.addAction(LineoutBroadcastReceiver.ERROR_ACTION);
        intentFilter.addAction(LineoutBroadcastReceiver.ADD_MESSAGE_ACTION);
        intentFilter.addAction(LineoutBroadcastReceiver.GET_MESSAGES_ACTION);

        publicKey = retIntent.getByteArrayExtra(SignupActivity.KEY_NAME);
        cryptoManager = (CryptoManager) retIntent.getSerializableExtra(SignupActivity.CRYPT_NAME);
        serializer = new PacketSerializer(cryptoManager);
        connectionSocket = lineoutApp.getMasterSocket();

        messageET = findViewById(R.id.message_et);
        sendBtn = findViewById(R.id.send_button);

        friendNameTV = findViewById(R.id.chat_name_tv);
        chatId = retIntent.getIntExtra(MenuActivity.CHAT_ID, -1);
        if(chatId != -1) {
            chats = wrapperSQL.getChats();
            for (HashMap<String, Object> chat: chats) {
                if(chatId == (Integer) chat.get("CHAT_ID"))
                {
                    chatName = (String) chat.get("CHAT_NAME");
                    friendNameTV.setText(chatName);
                    break;
                }
            }
        }

        broadcastReceiver = new LineoutBroadcastReceiver(wrapperSQL, cryptoManager, publicKey);
        lineoutApp.setLineoutBroadcastReceiver(broadcastReceiver, intentFilter);
        msgList = findViewById(R.id.msg_list);
        chatArrayAdapter = new ChatArrayAdapter(getApplicationContext(), R.layout.right_msg);
        msgList.setAdapter(chatArrayAdapter);
        msgList.setTranscriptMode(AbsListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
        msgList.setAdapter(chatArrayAdapter);
        chatArrayAdapter.registerDataSetObserver(new DataSetObserver() {
            @Override
            public void onChanged() {
                super.onChanged();
                msgList.setSelection(chatArrayAdapter.getCount() - 1);
            }
        });
        startRefreshTimer();

        ttsManager = new TextToSpeech(this, i -> {
            int result;
            if (i == TextToSpeech.SUCCESS)
            {
                result = ttsManager.setLanguage(Locale.ENGLISH);

                if(result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_NOT_SUPPORTED)
                {
                    isTTSEnabled = false;
                }
            }
            else
            {
                isTTSEnabled = false;
            }
        });

        msgList.setOnItemClickListener((parent, view, position, id) -> {
            if(isTTSEnabled)
            {
                ttsManager.speak(chatArrayAdapter.getItem(position).message, TextToSpeech.QUEUE_FLUSH, null, null);
            }
        });

        sendBtn.setOnClickListener(this::onClickSend);
    }

    /**
     * This function handles clicks on the send button.
     * @param view The current view (View).
     */
    public void onClickSend(View view)
    {
        PacketSerializer.Packet addMsgPacket = new PacketSerializer.Packet();
        HashMap<String, Object> userFields = wrapperSQL.getUserData();
        HashMap<String, Object> friendFields = wrapperSQL.getFriendData(chatName);
        ArrayList<Byte> publicKeyRSA = new ArrayList<>();
        HashMaker mask;
        byte[] signature = {0x30};
        byte[] hash, encryptedHash;
        byte[] destKey;
        long timeSent;
        int i;

        if (!messageET.getText().toString().equals("")) {
            addMsgPacket.opcode = PacketSerializer.ADD_MESSAGE_OPCODE;

            addMsgPacket.fields = new HashMap<>();
            addMsgPacket.fields.put("sender", userFields.get("USERNAME"));
            addMsgPacket.fields.put("content", messageET.getText().toString());
            timeSent = Instant.now().getEpochSecond();
            addMsgPacket.fields.put("timestamp", timeSent);

            try {
                mask = new HashMaker(MessageDigest.getInstance("SHA-256"));
                hash = mask.generateMask(messageET.getText().toString().getBytes(), MessageDigest.getInstance("SHA-256").getDigestLength());
                encryptedHash = cryptoManager.encryptData(CryptoManager.RSA_SIG_MODE, hash, publicKey);
                signature = Base64.getEncoder().encode(encryptedHash);
            } catch (Exception e) {
                e.printStackTrace();
            }

            addMsgPacket.fields.put("signature", new String(signature));

            for (byte b : publicKey) {
                publicKeyRSA.add(b);
            }
            addMsgPacket.fields.put("pub_key", publicKeyRSA);

            broadcastReceiver = new LineoutBroadcastReceiver(wrapperSQL, cryptoManager, messageET.getText().toString(), timeSent, (Integer) userFields.get("ID"), chatId);
            lineoutApp.setLineoutBroadcastReceiver(broadcastReceiver, intentFilter);

            try {
                publicKeyRSA = (ArrayList<Byte>) friendFields.get("PUBLIC_KEY");
                destKey = new byte[publicKeyRSA.size()];
                for (i = 0; i < publicKeyRSA.size(); i++) {
                    destKey[i] = publicKeyRSA.get(i);
                }
                lineoutWriter = new LineoutWriter(connectionSocket.getOutputStream(), serializer, addMsgPacket, chatName, destKey);
            } catch (IOException e) {
                e.printStackTrace();
            }

            writerThread = new Thread(lineoutWriter);
            writerThread.start();
        }
        else
        {
            Toast.makeText(getApplicationContext(), "Can't send empty messages", Toast.LENGTH_SHORT).show();
        }

    }

    /**
     * This functions stops the TTS manager and shuts it down when leaving the activity.
     */
    @Override
    protected void onDestroy() {
        if(ttsManager != null)
        {
            ttsManager.stop();
            ttsManager.shutdown();
        }
        super.onDestroy();
    }

    /**
     * This function resumes the timer if it is not running when the activity is resumed.
     */
    @Override
    public void onResume()
    {
        super.onResume();

        if(!timerWorking)
        {
            startRefreshTimer();
        }
    }


    /**
     * This function inflates the Lineout side menu.
     * @param menu The menu to inflate (Menu).
     * @return true (boolean).
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.extra_options_menu, menu);
        return true;
    }

    /**
     * This function changes the activity based on the menu item selected.
     * @param item The menu item selected by the user (MenuItem).
     * @return true (boolean).
     */
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int chosenOption = item.getItemId();
        Intent intent;

        if (chosenOption == R.id.add_friend) {
            intent = new Intent(ChatActivity.this, AddFriendActivity.class);
            intent.putExtra(SignupActivity.CRYPT_NAME, cryptoManager);
            intent.putExtra(SignupActivity.KEY_NAME, publicKey);
            stopRefreshTimer();
            startActivity(intent);
        }

        return true;
    }


    /**
     * This function stops the refresh timer & thread.
     */
    private void stopRefreshTimer(){
        if(refreshTimer != null){
            refreshTimer.cancel();
            refreshTimer.purge();
            timerWorking = false;
        }
    }

    /**
     * This function starts the refresh timer & thread.
     */
    private void startRefreshTimer(){
        timerWorking = true;
        refreshTimer = new Timer();
        refreshTask = new TimerTask() {
            public void run() {
                refreshHandler.post(() -> {
                    PacketSerializer.Packet getMessagesPacket = new PacketSerializer.Packet();
                    ChatMessage currentMsg;
                    String msg;
                    Integer senderId, userId;

                    getMessagesPacket.opcode = PacketSerializer.GET_MESSAGES_OPCODE;
                    try {
                        lineoutWriter = new LineoutWriter(connectionSocket.getOutputStream(), getMessagesPacket);
                        writerThread = new Thread(lineoutWriter);
                        writerThread.start();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    if(wrapperSQL.getMessages(chatId).size() == 0)
                    {
                        return;
                    }
                    Log.d("TAG", "run: HERE");

                    if(wrapperSQL.getMessages(chatId).size() != chatArrayAdapter.getCount())
                    {
                        chatArrayAdapter.clear();
                        messages = wrapperSQL.getMessages(chatId);
                        userId = (Integer) wrapperSQL.getUserData().get("ID");
                        for (HashMap<String, Object> message: messages) {
                            senderId = (Integer) message.get("SENDER_ID");
                            msg = (String) message.get("MSG_CONTENT");
                            currentMsg = new ChatMessage((!Objects.equals(userId, senderId)), msg);
                            chatArrayAdapter.add(currentMsg);
                        }
                    }
                });
            }
        };
        refreshTimer.schedule(refreshTask, 1, 10000);
    }
}