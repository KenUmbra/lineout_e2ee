package com.ken.lineout.gui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.ken.lineout.R;

import java.util.ArrayList;
import java.util.List;

public class ChatArrayAdapter extends ArrayAdapter<ChatMessage> {

    private TextView chatText;
    private List<ChatMessage> chatMessageList = new ArrayList<>();
    private Context context;

    /**
     * This function creates an instance of a ChatArrayAdapter object.
     * @param context The context of the current activity (Context).
     * @param textViewResourceId The resource id of the text view (int).
     */
    public ChatArrayAdapter(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
        this.context = context;
    }

    /**
     * This function adds a chat message to the chat message list.
     * @param msg The message to add (ChatMessage).
     */
    @Override
    public void add(ChatMessage msg) {
        chatMessageList.add(msg);
        super.add(msg);
    }

    /**
     * This function counts how many messages are displayed.
     * @return The amount of displayed messages (int).
     */
    public int getCount() {
        return this.chatMessageList.size();
    }

    /**
     * This function retrieves a particular chat message.
     * @param index The index of the chat message (int).
     * @return The chat message object that corresponds to the index (ChatMessage).
     */
    public ChatMessage getItem(int index) {
        return this.chatMessageList.get(index);
    }

    /**
     * This function clears the message list.
     */
    public void clear()
    {
        chatMessageList.clear();
        super.clear();
    }

    /**
     * This function displays the chat messages stored in the message list.
     * @param position The position of the view (int).
     * @param convertView The view itself (convertView).
     * @param parent The parent of the view (ViewGroup).
     * @return The new view (View).
     */
    public View getView(int position, View convertView, ViewGroup parent) {
        ChatMessage chatMessageObj = getItem(position);
        View row;
        LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (chatMessageObj.left) {
            row = inflater.inflate(R.layout.left_msg, parent, false);
        }else{
            row = inflater.inflate(R.layout.right_msg, parent, false);
        }
        chatText = (TextView) row.findViewById(R.id.msg_box);
        chatText.setText(chatMessageObj.message);
        return row;
    }
}
