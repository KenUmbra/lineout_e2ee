package com.ken.lineout.encryptions;

import java.io.Serializable;

public class CryptoManager implements Serializable
{
    public static final String AES_MODE = "AES";
    public static final String RSA_MODE = "RSA";
    public static final String RSA_SIG_MODE = "RSA_SIG";

    private final EncryptAES eAes;
    private final DecryptAES dAes;
    private final EncryptRSA eRsa;
    private final DecryptRSA dRsa;
    private final EncryptSignatureRSA eSigRSA;
    private final DecryptSignatureRSA dSigRSA;
    
    /**
     * This constructor creates a CryptoManager object & initializes it.
     * @param privateKeyRSA The user's private RSA key (byte[]).
     */
    public CryptoManager(byte[] privateKeyRSA)
    {
        this.eAes = new EncryptAES();
        this.dAes = new DecryptAES();
        this.eRsa = new EncryptRSA();
        this.dRsa = new DecryptRSA(privateKeyRSA);
        this.eSigRSA = new EncryptSignatureRSA(privateKeyRSA);
        this.dSigRSA = new DecryptSignatureRSA(privateKeyRSA);
    }

    /**
     * This function encrypts given data using AES or RSA, depending on the mode chosen.
     * @param mode The encryption method chosen (String).
     * @param data The data to encrypt (byte[]).
     * @param key The AES key in case of AES, and the RSA public key in case of RSA (byte[]).
     * @return The encrypted data (byte[]).
     * @throws Exception if the mode inputted is not supported.
     */
    public byte[] encryptData(String mode, byte[] data, byte[] key) throws Exception
    {
        byte[] encryptedData;
        switch (mode) {
            case AES_MODE:
                encryptedData = eAes.encrypt(key, data);
                break;
            case RSA_MODE:
                encryptedData = eRsa.encrypt(key, data);
                break;
            case RSA_SIG_MODE:
                encryptedData = eSigRSA.encrypt(key, data);
                break;
            default:
                throw new Exception("Unrecognized Mode");
        }

        return encryptedData;
    }

    /**
     * This function decrypts given data using AES or RSA, depending on the mode chosen.
     * @param mode The decryption of the given data (String).
     * @param data The data to decrypt (byte[]).
     * @param key The AES key in case of AES, and the RSA public key in case of RSA (byte[]).
     * @return The decrypted data (byte[]).
     * @throws Exception if the mode inputted is not supported.
     */
    public byte[] decryptData(String mode, byte[] data, byte[] key) throws Exception
    {
        byte[] decryptedData;
        switch (mode) {
            case AES_MODE:
                decryptedData = dAes.decrypt(key, data);
                break;
            case RSA_MODE:
                decryptedData = dRsa.decrypt(key, data);
                break;
            case RSA_SIG_MODE:
                decryptedData = dSigRSA.decrypt(key, data);
                break;
            default:
                throw new Exception("Unrecognized Mode");
        }

        return decryptedData;
    }
}