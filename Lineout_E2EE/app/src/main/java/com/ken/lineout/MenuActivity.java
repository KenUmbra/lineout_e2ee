package com.ken.lineout;

import android.content.Intent;
import android.content.IntentFilter;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.ken.lineout.database.DatabaseWrapperSQL;
import com.ken.lineout.encryptions.CryptoManager;
import com.ken.lineout.networking.LineoutApp;
import com.ken.lineout.networking.LineoutBroadcastReceiver;
import com.ken.lineout.networking.LineoutWriter;
import com.ken.lineout.packet_handling.PacketSerializer;

import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

public class MenuActivity extends AppCompatActivity {
    public static final String CHAT_ID = "ChatID";

    private CryptoManager cryptoManager;

    private DatabaseWrapperSQL wrapperSQL;

    private IntentFilter intentFilter = new IntentFilter();
    private LineoutBroadcastReceiver broadcastReceiver = null;
    private LineoutWriter lineoutWriter;
    private Thread writerThread;
    private Socket connectionSocket;

    private Timer refreshTimer;
    private TimerTask refreshTask;
    private Handler refreshHandler = new Handler();
    private ArrayList<HashMap<String, Object>> chats = new ArrayList<>();
    private ArrayList<String> chatNames = new ArrayList<>();
    private ArrayAdapter<String> adapter;

    private byte[] publicKey;
    private boolean timerWorking = false;

    private ListView chatList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        LineoutApp lineoutApp = (LineoutApp) getApplication();
        Intent retIntent = getIntent();
        SQLiteDatabase db = openOrCreateDatabase(DatabaseWrapperSQL.DB_FILE_NAME, MODE_PRIVATE, null);
        wrapperSQL = new DatabaseWrapperSQL(db);

        intentFilter.addAction(LineoutBroadcastReceiver.ERROR_ACTION);
        intentFilter.addAction(LineoutBroadcastReceiver.GET_MESSAGES_ACTION);

        publicKey = retIntent.getByteArrayExtra(SignupActivity.KEY_NAME);
        cryptoManager = (CryptoManager) retIntent.getSerializableExtra(SignupActivity.CRYPT_NAME);
        connectionSocket = lineoutApp.getMasterSocket();

        broadcastReceiver = new LineoutBroadcastReceiver(wrapperSQL, cryptoManager, publicKey);
        lineoutApp.setLineoutBroadcastReceiver(broadcastReceiver, intentFilter);

        chatList = findViewById(R.id.chat_list);
        adapter = new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_list_item_1, chatNames);
        chatList.setAdapter(adapter);
        startRefreshTimer();

        chatList.setOnItemClickListener((parent, view, position, id) -> {
            Intent intent = new Intent(MenuActivity.this, ChatActivity.class);
            intent.putExtra(CHAT_ID, position + 1);
            intent.putExtra(SignupActivity.CRYPT_NAME, cryptoManager);
            intent.putExtra(SignupActivity.KEY_NAME, publicKey);
            stopRefreshTimer();
            startActivity(intent);
        });

    }

    /**
     * This function resumes the timer if it is not running when the activity is resumed.
     */
    @Override
    public void onResume()
    {
        super.onResume();

        if(!timerWorking)
        {
            startRefreshTimer();
        }
    }


    /**
     * This function inflates the Lineout side menu.
     * @param menu The menu to inflate (Menu).
     * @return true (boolean).
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.extra_options_menu, menu);
        return true;
    }

    /**
     * This function changes the activity based on the menu item selected.
     * @param item The menu item selected by the user (MenuItem).
     * @return true (boolean).
     */
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int chosenOption = item.getItemId();
        Intent intent;

        if (chosenOption == R.id.add_friend) {
            intent = new Intent(MenuActivity.this, AddFriendActivity.class);
            intent.putExtra(SignupActivity.CRYPT_NAME, cryptoManager);
            intent.putExtra(SignupActivity.KEY_NAME, publicKey);
            stopRefreshTimer();
            startActivity(intent);
        }

        return true;
    }


    /**
     * This function stops the refresh timer & thread.
     */
    private void stopRefreshTimer(){
        if(refreshTimer != null){
            refreshTimer.cancel();
            refreshTimer.purge();
            timerWorking = false;
        }
    }

    /**
     * This function starts the refresh timer & thread.
     */
    private void startRefreshTimer(){
        timerWorking = true;
        refreshTimer = new Timer();
        refreshTask = new TimerTask() {
            public void run() {
                refreshHandler.post(() -> {
                    PacketSerializer.Packet getMessagesPacket = new PacketSerializer.Packet();
                    getMessagesPacket.opcode = PacketSerializer.GET_MESSAGES_OPCODE;

                    try {
                        lineoutWriter = new LineoutWriter(connectionSocket.getOutputStream(), getMessagesPacket);
                        writerThread = new Thread(lineoutWriter);
                        writerThread.start();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    if(wrapperSQL.countChats() == 0)
                    {
                        return;
                    }

                    if(wrapperSQL.countChats() != chatNames.size())
                    {
                        chatNames.clear();
                        chats = wrapperSQL.getChats();
                        for (HashMap<String, Object> chat: chats) {
                            chatNames.add((String) chat.get("CHAT_NAME"));
                        }
                        adapter = new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_list_item_1, chatNames);
                        chatList.setAdapter(adapter);
                    }
                });
            }
        };
        refreshTimer.schedule(refreshTask, 1, 10000);
    }
}