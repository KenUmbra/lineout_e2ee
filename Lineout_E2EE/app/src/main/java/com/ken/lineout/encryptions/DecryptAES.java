package com.ken.lineout.encryptions;

import java.io.Serializable;

import com.ken.lineout.encryptions.lineout_key_generators.*;

public class DecryptAES implements IDecrypt, Serializable
{
    private final UtilityAES UTILITY_AES = new UtilityAES();

    /**
     * This function does Inverse S-BOX transformation to each byte in the word.
     * @param word The word to undergo Inverse S-BOX transformation (byte[]).
     * @return The word after Inverse S-BOX transformation (byte[]).
     */
    private byte[] inverseSubWord(byte[] word)
    {
        int i;
        byte[] temp = new byte[word.length];

        for (i = 0; i < word.length; i++)
        {
            temp[i] = UtilityAES.INVERSE_S_BOX[(int)(word[i] & 0xff) / 16][(int)(word[i] & 0xff) % 16];
        }

        return temp;
    }

    /**
     * This function performs Inverse S-BOX transformation to the state block.
     * @param state The state block (byte[][]).
     */
    private void inverseSubBytes(byte[][] state)
    {
        int i;
        
        for(i = 0; i < state.length; i++)
        {
            state[i] = inverseSubWord(state[i]);
        }

    }

    /**
     * This function shifts the rows in the state based on Rijndael Inverse ShiftRows.
     * @param state The state block (byte[][]).
     */
    private void inverseShiftRows(byte[][] state)
    {
        int i, j;
        for(i = 1; i < UtilityAES.WORD_IN_BYTES; i++)
        {
            for(j = UtilityAES.WORD_IN_BYTES - i; j > 0; j--)
            {
                state[i] = UTILITY_AES.rotateWord(state[i]);
            }
        }
    }

    /**
     * This function mixes the columns inversely in the state block.
     * @param state The state block (byte[][]).
     */
    private void inverseMixColumns(byte[][] state)
    {
        byte[] temp = new byte[UtilityAES.WORD_IN_BYTES];
        byte[] tempPostMix = new byte[UtilityAES.WORD_IN_BYTES];
        int i, j;

        for(i = 0; i < UtilityAES.WORD_IN_BYTES; i++)
        {
            for(j = 0; j < UtilityAES.WORD_IN_BYTES; j++)
            {
                temp[j] = state[j][i];
            }

            tempPostMix[0] = (byte) ((int) (UTILITY_AES.mulBytes((byte) 0x0e, temp[0]) & 0xff) ^ (int) (UTILITY_AES.mulBytes((byte) 0x0b, temp[1]) & 0xff) ^ (int) (UTILITY_AES.mulBytes((byte) 0x0d, temp[2]) & 0xff) ^ (int) (UTILITY_AES.mulBytes((byte) 0x09, temp[3]) & 0xff));
            tempPostMix[1] = (byte) ((int) (UTILITY_AES.mulBytes((byte) 0x09, temp[0]) & 0xff) ^ (int) (UTILITY_AES.mulBytes((byte) 0x0e, temp[1]) & 0xff) ^ (int) (UTILITY_AES.mulBytes((byte) 0x0b, temp[2]) & 0xff) ^ (int) (UTILITY_AES.mulBytes((byte) 0x0d, temp[3]) & 0xff));
            tempPostMix[2] = (byte) ((int) (UTILITY_AES.mulBytes((byte) 0x0d, temp[0]) & 0xff) ^ (int) (UTILITY_AES.mulBytes((byte) 0x09, temp[1]) & 0xff) ^ (int) (UTILITY_AES.mulBytes((byte) 0x0e, temp[2]) & 0xff) ^ (int) (UTILITY_AES.mulBytes((byte) 0x0b, temp[3]) & 0xff));
            tempPostMix[3] = (byte) ((int) (UTILITY_AES.mulBytes((byte) 0x0b, temp[0]) & 0xff) ^ (int) (UTILITY_AES.mulBytes((byte) 0x0d, temp[1]) & 0xff) ^ (int) (UTILITY_AES.mulBytes((byte) 0x09, temp[2]) & 0xff) ^ (int) (UTILITY_AES.mulBytes((byte) 0x0e, temp[3]) & 0xff));

            for(j = 0; j < UtilityAES.WORD_IN_BYTES; j++)
            {
                state[j][i] = tempPostMix[j];
            }
        }

    }
    
    /**
     * This function decodes a block of information using AES.
     * @param input The input to be decrypted by the function (byte[]).
     * @param roundKeys The round keys (byte[]).
     * @return The decrypted output (byte[]).
     */
    private byte[] decryptBlock(byte[] input, byte[] roundKeys)
    {
        byte[][] state = new byte[UtilityAES.WORD_IN_BYTES][UtilityAES.WORD_IN_BYTES];
        byte[] output = new byte[KeyGenerator.AES_KEY_SIZE_BYTES];
        byte[] currentKey = new byte[KeyGenerator.AES_KEY_SIZE_BYTES];
        int i, j, round;

        for(i = 0; i < state.length; i++)
        {
            for(j = 0; j < state[0].length; j++)
            {
                state[i][j] = input[i + UtilityAES.WORD_IN_BYTES * j];
            }
        }

        System.arraycopy(roundKeys, UtilityAES.AMOUNT_OF_ROUNDS * KeyGenerator.AES_KEY_SIZE_BYTES, currentKey, 0, KeyGenerator.AES_KEY_SIZE_BYTES);
        state = UTILITY_AES.addRoundKey(state, currentKey);

        for(round = UtilityAES.AMOUNT_OF_ROUNDS - 1; round >= 1; round--)
        {
            inverseShiftRows(state);
            inverseSubBytes(state);

            System.arraycopy(roundKeys, round * KeyGenerator.AES_KEY_SIZE_BYTES, currentKey, 0, KeyGenerator.AES_KEY_SIZE_BYTES);
            state = UTILITY_AES.addRoundKey(state, currentKey);
            inverseMixColumns(state);
        }

        inverseShiftRows(state);
        inverseSubBytes(state);

        System.arraycopy(roundKeys, 0, currentKey, 0, KeyGenerator.AES_KEY_SIZE_BYTES);
        state = UTILITY_AES.addRoundKey(state, currentKey);
        
        for(i = 0; i < UtilityAES.WORD_IN_BYTES; i++)
        {
            for(j = 0; j < UtilityAES.WORD_IN_BYTES; j++)
            {
                output[i + UtilityAES.WORD_IN_BYTES * j] = state[i][j];
            }
        }

        return output;
    }

    /**
     * This function decrypts input using AES Encryption.
     * @param key The AES key used used for decryption (byte[]).
     * @param input The input to be decrypted by the function (byte[]).
     * @return The decrypted input (byte[]).
     */
    public byte[] decrypt(byte[] key, byte[] input)
    {
        byte[] temp = new byte[KeyGenerator.AES_KEY_SIZE_BYTES];
        byte[] output = new byte[input.length];
        byte[] roundKeys = UTILITY_AES.keyExpansion(key);
        int i;

        for(i = 0; i < output.length; i += KeyGenerator.AES_KEY_SIZE_BYTES)
        {
            System.arraycopy(input, i, temp, 0, KeyGenerator.AES_KEY_SIZE_BYTES);
            System.arraycopy(decryptBlock(temp, roundKeys), 0, output, i, KeyGenerator.AES_KEY_SIZE_BYTES);
        }

        return output;
    }
}
