package com.ken.lineout.networking;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.ken.lineout.encryptions.CryptoManager;
import com.ken.lineout.encryptions.lineout_key_generators.KeyGenerator;
import com.ken.lineout.packet_handling.PacketDeserializer;

import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.math.BigInteger;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.util.ArrayList;

public class LineoutReader extends Thread implements Serializable {
    private static final String TAG = "LineoutReader";

    private static final int CODE_RECV_SIZE = 1;
    private static final int KEY_SIZE_RECV_SIZE = 1;
    private static final int DATA_SIZE_RECV_SIZE = 2;
    private static final int DEFAULT_SIZE = 1024;

    private final Socket socket;
    private final InputStream inputStream;
    private final PacketDeserializer deserializer;
    private final Context context;

    /**
     * This function initializes an instance of a Lineout Reader object.
     * @param socket The server's socket (Socket).
     * @param inputStream The sockets input stream (Input Stream).
     * @param deserializer The user's packet deserializer (PacketDeserializer).
     */
    public LineoutReader(Socket socket, InputStream inputStream, PacketDeserializer deserializer, Context context)
    {
        this.socket = socket;
        this.inputStream = inputStream;
        this.deserializer = deserializer;
        this.context = context;
    }

    /**
     * This function fetches data sent by the server & handles it.
     */
    @Override
    public void run() {
        byte[] code = new byte[CODE_RECV_SIZE];
        int size;

        if(this.socket != null) {
            while (true) {
                try {
                    size = this.inputStream.read(code);
                    if (size == CODE_RECV_SIZE) {
                        if(PacketDeserializer.SIGNUP_RESULT_OPCODE <= code[0] && code[0] <= PacketDeserializer.GET_MESSAGES_RESULT_OPCODE) {
                            Log.d(TAG, "run: CODE: " + code[0]);
                            parsePacket(code[0]);
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    if (this.isInterrupted()) {
                        return;
                    }
                }
            }
        }
    }

    /**
     * This function parses the data received from the server.
     * @param opcode The packet's opcode.
     */
    private void parsePacket(byte opcode)
    {
        Intent intent;
        String errorMsg;
        byte[] sizeBuffer, errorBuffer;
        byte[] aesKey, encryptedData;
        boolean resultCode;
        int size;

        if(opcode == PacketDeserializer.ERROR_OPCODE)
        {
            try {
                errorBuffer = new byte[DEFAULT_SIZE];
                size = inputStream.read(errorBuffer);

                if(size == errorBuffer.length)
                {
                    errorMsg = deserializer.deserializeErrorPacket(errorBuffer);
                    intent = new Intent();
                    intent.setAction(LineoutBroadcastReceiver.ERROR_ACTION);
                    intent.putExtra("data", errorMsg);
                    context.sendBroadcast(intent);
                }
            } catch (IOException e) {
                e.printStackTrace();
                Log.d(TAG, "parsePacket: Couldn't Fetch Packet");
            } catch (Exception e) {
                e.printStackTrace();
                Log.d(TAG, "parsePacket: Couldn't Deserialize Packet");
            }
            return;
        }

        try {
            sizeBuffer = new byte[KEY_SIZE_RECV_SIZE];
            size = inputStream.read(sizeBuffer);
            if (size != sizeBuffer.length) {
                Log.d(TAG, "parsePacket: Wrong Size 00");
                return;
            }

            if(((KeyGenerator.RSA_KEY_SIZE_BITS / 8) + 1) < (sizeBuffer[0] & 0xFF))
            {
                Log.d(TAG, "parsePacket: Wrong Size 015");
                return;
            }

            aesKey = new byte[(sizeBuffer[0] & 0xFF)];
            size = inputStream.read(aesKey);
            if (size != aesKey.length) {
                Log.d(TAG, "parsePacket: Wrong Size 01");
                return;
            }

            sizeBuffer = new byte[DATA_SIZE_RECV_SIZE];
            size = inputStream.read(sizeBuffer);
            if (size != sizeBuffer.length) {
                Log.d(TAG, "parsePacket: Wrong Size 02");
                return;
            }

            encryptedData = new byte[new BigInteger(1, sizeBuffer).intValue()];
            size = inputStream.read(encryptedData);
            if (size != encryptedData.length) {
                Log.d(TAG, "parsePacket: Wrong Size 03");
                return;
            }

            aesKey = deserializer.getCryptoManager().decryptData(CryptoManager.RSA_MODE, aesKey, deserializer.getUserPubKeyRSA());
            if(aesKey.length == 1)
            {
                Log.d(TAG, "parsePacket: Wrong Size 035");
                return;
            }

            switch (opcode) {
                case PacketDeserializer.SIGNUP_RESULT_OPCODE:
                    resultCode = deserializer.deserializeActionPacket(encryptedData, aesKey);
                    intent = new Intent();
                    intent.setAction(LineoutBroadcastReceiver.SIGNUP_ACTION);
                    intent.putExtra("data", resultCode);
                    context.sendBroadcast(intent);
                    Log.d(TAG, "parsePacket: " + resultCode);
                    break;

                case PacketDeserializer.LOGIN_RESULT_OPCODE:
                    resultCode = deserializer.deserializeActionPacket(encryptedData, aesKey);
                    intent = new Intent();
                    intent.setAction(LineoutBroadcastReceiver.LOGIN_ACTION);
                    intent.putExtra("data", resultCode);
                    context.sendBroadcast(intent);
                    Log.d(TAG, "parsePacket: " + resultCode);
                    break;

                case PacketDeserializer.ADD_FRIEND_RESULT_OPCODE:
                    byte[] rsaKey = deserializer.deserializeGetKeyPacket(encryptedData, aesKey);
                    intent = new Intent();
                    intent.setAction(LineoutBroadcastReceiver.ADD_FRIEND_ACTION);
                    intent.putExtra("data", rsaKey);
                    context.sendBroadcast(intent);
                    Log.d(TAG, "parsePacket: " + rsaKey.length);
                    break;

                case PacketDeserializer.ADD_MESSAGE_RESULT_OPCODE:
                    resultCode = deserializer.deserializeActionPacket(encryptedData, aesKey);
                    intent = new Intent();
                    intent.setAction(LineoutBroadcastReceiver.ADD_MESSAGE_ACTION);
                    intent.putExtra("data", resultCode);
                    context.sendBroadcast(intent);
                    Log.d(TAG, "parsePacket: " + resultCode);
                    break;

                case PacketDeserializer.GET_MESSAGES_RESULT_OPCODE:
                    ArrayList<JSONObject> messages = deserializer.deserializeGetMessagesPacket(encryptedData, aesKey);
                    ArrayList<String> packableMessages = new ArrayList<>();
                    for (JSONObject msg: messages) {
                        packableMessages.add(msg.toString());
                    }
                    intent = new Intent();
                    intent.setAction(LineoutBroadcastReceiver.GET_MESSAGES_ACTION);
                    intent.putExtra("data", packableMessages);
                    context.sendBroadcast(intent);
                    Log.d(TAG, "parsePacket: " + packableMessages);
                    break;

                default:
                    Log.d(TAG, "parsePacket: Unknown Code");
                    break;
            }
        } catch (IOException e) {
            e.printStackTrace();
            Log.d(TAG, "parsePacket: Couldn't Fetch Packet");
        } catch (Exception e) {
            e.printStackTrace();
            Log.d(TAG, "parsePacket: Couldn't Deserialize Packet");
        }

    }

}
