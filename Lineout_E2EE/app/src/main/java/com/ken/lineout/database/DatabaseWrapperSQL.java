package com.ken.lineout.database;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import androidx.annotation.NonNull;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

public class DatabaseWrapperSQL implements Serializable {
    public static final String[] TABLE_NAMES = {"FRIENDS", "CHATS", "MESSAGES"};
    public static final String DB_FILE_NAME = "LineoutDB.sqlite";

    // Table Creation Constants:
    private static final String FRIENDS_TABLE_CREATION_SQL = "CREATE TABLE IF NOT EXISTS FRIENDS(ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " +
            "USERNAME VARCHAR(32) NOT NULL, PUBLIC_KEY BINARY NOT NULL, PRIVATE_KEY BINARY);";
    private static final String CHATS_TABLE_CREATION_SQL = "CREATE TABLE IF NOT EXISTS CHATS(CHAT_ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " +
            "CHAT_NAME VARCHAR(32) NOT NULL, MEM1_ID INTEGER NOT NULL);";
    private static final String MESSAGES_TABLE_CREATION_SQL = "CREATE TABLE IF NOT EXISTS MESSAGES(MSG_ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " +
            "MSG_CONTENT TEXT NOT NULL, TIME_SENT BIGINT NOT NULL, CHAT_ID INTEGER NOT NULL, SENDER_ID INTEGER NOT NULL);";

    // Table Insertion Constants:
    private static final String FRIENDS_TABLE_INSERT_USER_SQL = "INSERT INTO FRIENDS (USERNAME, PUBLIC_KEY, PRIVATE_KEY) VALUES (?, ?, ?)";
    private static final String FRIENDS_TABLE_INSERT_FRIEND_SQL = "INSERT INTO FRIENDS (USERNAME, PUBLIC_KEY) VALUES (?, ?)";
    private static final String CHATS_TABLE_INSERT_CHAT_SQL = "INSERT INTO CHATS (CHAT_NAME, MEM1_ID) VALUES (?, ?)";
    private static final String MESSAGES_TABLE_INSERT_MSG_SQL = "INSERT INTO MESSAGES (MSG_CONTENT, TIME_SENT, CHAT_ID, SENDER_ID) VALUES (?, ?, ?, ?)";

    // Table Count Constants:
    private static final String FRIENDS_TABLE_QUERY_COUNT_SQL = "SELECT COUNT(DISTINCT ID) FROM FRIENDS";
    private static final String CHATS_TABLE_QUERY_COUNT_SQL = "SELECT COUNT(DISTINCT CHAT_ID) FROM CHATS";
    private static final String MESSAGES_TABLE_QUERY_COUNT_SQL = "SELECT COUNT(DISTINCT CHAT_ID) FROM MESSAGES";

    // Table Queries Constants - Friends:
    private static final String[] FRIENDS_TABLE_QUERY_USER_DATA_COLUMNS_SQL = {"ID", "USERNAME", "PUBLIC_KEY", "PRIVATE_KEY"};
    private static final String[] FRIENDS_TABLE_QUERY_FRIEND_ID_DATA_COLUMNS_SQL = {"ID", "PUBLIC_KEY"};
    private static final String[] FRIENDS_TABLE_QUERY_FRIEND_USERNAME_DATA_COLUMNS_SQL = {"USERNAME", "PUBLIC_KEY"};
    private static final String FRIENDS_TABLE_QUERY_USER_DATA_SQL = "SELECT * FROM FRIENDS WHERE PRIVATE_KEY NOT NULL LIMIT 1";
    private static final String FRIENDS_TABLE_QUERY_FRIEND_USERNAME_DATA_SQL = "SELECT (USERNAME, PUBLIC_KEY) FROM FRIENDS WHERE ID = ";
    private static final String FRIENDS_TABLE_QUERY_FRIEND_ID_DATA_SELECTION_SQL = "USERNAME = ?";

    // Table Queries Constants - Chats:
    private static final String[] CHATS_TABLE_QUERY_USER_DATA_COLUMNS_SQL = {"CHAT_ID", "CHAT_NAME", "MEM1_ID"};
    private static final String CHATS_TABLE_QUERY_ALL_CHAT_DATA_SQL = "SELECT * FROM CHATS";

    // Table Queries Constants - Messages:
    private static final String[] MESSAGES_TABLE_QUERY_USER_DATA_COLUMNS_SQL = {"MSG_ID", "MSG_CONTENT", "TIME_SENT", "CHAT_ID", "SENDER_ID"};
    private static final String MESSAGES_TABLE_QUERY_ALL_CHAT_DATA_SQL = "SELECT * FROM MESSAGES WHERE CHAT_ID = ";
    private static final String MESSAGES_TABLE_QUERY_ORDER_BY_TIME = " ORDER BY TIME_SENT";

    private static final String LIMIT_ONE = "1";

    private static final int ACTION_FAILED = -1;

    private final SQLiteDatabase sqliteDB;

    /**
     * This function initializes a SqliteDatabaseWrapper object.
     * @param sqliteDB The database to wrap (SQLiteDatabase).
     */
    public DatabaseWrapperSQL(@NonNull SQLiteDatabase sqliteDB)
    {
        this.sqliteDB = sqliteDB;
        sqliteDB.execSQL(FRIENDS_TABLE_CREATION_SQL);
        sqliteDB.execSQL(CHATS_TABLE_CREATION_SQL);
        sqliteDB.execSQL(MESSAGES_TABLE_CREATION_SQL);
    }

    /**
     * This function inserts a new system user into the local database.
     * @param username The user's name (String).
     * @param publicKey The user's RSA public key (byte[]).
     * @param privateKey The user's RSA private key (byte[]).
     * @return true - Action Successful, false - Action Failed (boolean).
     */
    public boolean insertUser(String username, byte[] publicKey, byte[] privateKey)
    {
        SQLiteStatement statement = sqliteDB.compileStatement(FRIENDS_TABLE_INSERT_USER_SQL);
        statement.bindString(1, username);
        statement.bindBlob(2, publicKey);
        statement.bindBlob(3, privateKey);

        return ACTION_FAILED != statement.executeInsert();
    }

    /**
     * This function inserts a new friend into the local database.
     * @param username The friend's username (String).
     * @param publicKey The friend's RSA public key (byte[]).
     * @return true - Action Successful, false - Action Failed (boolean).
     */
    public boolean insertFriend(String username, byte[] publicKey)
    {
        SQLiteStatement statement = sqliteDB.compileStatement(FRIENDS_TABLE_INSERT_FRIEND_SQL);
        statement.bindString(1, username);
        statement.bindBlob(2, publicKey);

        return ACTION_FAILED != statement.executeInsert();
    }

    /**
     * This function inserts a new chat into the local database.
     * @param chatName The chat's name (String).
     * @param mem1ID The ID of the other chat member's (int).
     * @return true - Action Successful, false - Action Failed (boolean).
     */
    public boolean insertChat(String chatName, int mem1ID)
    {
        SQLiteStatement statement = sqliteDB.compileStatement(CHATS_TABLE_INSERT_CHAT_SQL);
        statement.bindString(1, chatName);
        statement.bindLong(2, mem1ID);

        return ACTION_FAILED != statement.executeInsert();
    }

    /**
     * This function inserts a new message into the local database.
     * @param msgContent The message's contents (String).
     * @param timestamp The unix time of when the message was sent (long).
     * @param chatID The ID of the chat the message will appear in (int).
     * @param senderID The ID of the message's sender (int).
     * @return true - Action Successful, false - Action Failed (boolean).
     */
    public boolean insertMessage(String msgContent, long timestamp, int chatID, int senderID)
    {
        SQLiteStatement statement = sqliteDB.compileStatement(MESSAGES_TABLE_INSERT_MSG_SQL);
        statement.bindString(1, msgContent);
        statement.bindLong(2, timestamp);
        statement.bindLong(3, chatID);
        statement.bindLong(4, senderID);

        return ACTION_FAILED != statement.executeInsert();
    }

    /**
     * This function counts the entries in the Friends table.
     * @return The amount of entries in the Friends table (long).
     */
    public long countFriends()
    {
        SQLiteStatement statement = sqliteDB.compileStatement(FRIENDS_TABLE_QUERY_COUNT_SQL);
        return statement.simpleQueryForLong();
    }

    /**
     * This function counts the entries in the Chats table.
     * @return The amount of entries in the Chats table (long).
     */
    public long countChats()
    {
        SQLiteStatement statement = sqliteDB.compileStatement(CHATS_TABLE_QUERY_COUNT_SQL);
        return statement.simpleQueryForLong();
    }

    /**
     * This function counts the entries in the Messages table.
     * @return The amount of entries in the Messages table (long).
     */
    public long countMessages()
    {
        SQLiteStatement statement = sqliteDB.compileStatement(MESSAGES_TABLE_QUERY_COUNT_SQL);
        return statement.simpleQueryForLong();
    }

    /**
     * This function fetches the data of the system user.
     * @return The entry of the system user (HashMap<String, Object>).
     */
    public HashMap<String, Object> getUserData()
    {
        Cursor userCursor = sqliteDB.rawQuery(FRIENDS_TABLE_QUERY_USER_DATA_SQL, null);
        HashMap<String, Object> userFields = new HashMap<>();
        ArrayList<Byte> keyObject;
        byte[] keyPrimitive;
        int i, j;

        while (userCursor.moveToNext())
        {
            for (i = 0; i < userCursor.getColumnCount(); i++) {
                if (userCursor.getColumnName(i).equals(FRIENDS_TABLE_QUERY_USER_DATA_COLUMNS_SQL[0])) {
                    userFields.put(userCursor.getColumnName(i), userCursor.getInt(i));
                }
                else if (userCursor.getColumnName(i).equals(FRIENDS_TABLE_QUERY_USER_DATA_COLUMNS_SQL[1]))
                {
                    userFields.put(userCursor.getColumnName(i), userCursor.getString(i));
                }
                else
                {
                    keyObject = new ArrayList<>();
                    keyPrimitive = userCursor.getBlob(i);
                    for(j = 0; j < keyPrimitive.length; j++)
                    {
                        keyObject.add(keyPrimitive[j]);
                    }
                    userFields.put(userCursor.getColumnName(i), keyObject);
                }
            }
        }

        userCursor.close();

        return userFields;
    }

    /**
     * This function fetches the data of a friend based on their username.
     * @param username The friend's username (String).
     * @return The entry of the friend (HashMap<String, Object>).
     */
    public HashMap<String, Object> getFriendData(String username)
    {
        String[] selectionArgs = { username };
        Cursor userCursor = sqliteDB.query(TABLE_NAMES[0],
                FRIENDS_TABLE_QUERY_FRIEND_ID_DATA_COLUMNS_SQL,
                FRIENDS_TABLE_QUERY_FRIEND_ID_DATA_SELECTION_SQL,
                selectionArgs,
                null,
                null,
                null,
                LIMIT_ONE);
        HashMap<String, Object> userFields = new HashMap<>();
        ArrayList<Byte> keyObject;
        byte[] keyPrimitive;
        int i, j;

        while (userCursor.moveToNext())
        {
            for (i = 0; i < userCursor.getColumnCount(); i++) {
                if (userCursor.getColumnName(i).equals(FRIENDS_TABLE_QUERY_FRIEND_ID_DATA_COLUMNS_SQL[0])) {
                    userFields.put(userCursor.getColumnName(i), userCursor.getInt(i));
                }
                else
                {
                    keyObject = new ArrayList<>();
                    keyPrimitive = userCursor.getBlob(i);
                    for(j = 0; j < keyPrimitive.length; j++)
                    {
                        keyObject.add(keyPrimitive[j]);
                    }
                    userFields.put(userCursor.getColumnName(i), keyObject);
                }
            }
        }

        userCursor.close();

        return userFields;
    }

    /**
     * This function fetches the data of a friend based on their id.
     * @param id The friend's id (int).
     * @return The entry of the friend (HashMap<String, Object>).
     */
    public HashMap<String, Object> getFriendData(int id)
    {
        Cursor userCursor = sqliteDB.rawQuery(FRIENDS_TABLE_QUERY_FRIEND_USERNAME_DATA_SQL + id + LIMIT_ONE, null);
        HashMap<String, Object> userFields = new HashMap<>();
        ArrayList<Byte> keyObject;
        byte[] keyPrimitive;
        int i, j;

        while (userCursor.moveToNext())
        {
            for (i = 0; i < userCursor.getColumnCount(); i++) {
                if (userCursor.getColumnName(i).equals(FRIENDS_TABLE_QUERY_FRIEND_USERNAME_DATA_COLUMNS_SQL[0])) {
                    userFields.put(userCursor.getColumnName(i), userCursor.getString(i));
                }
                else
                {
                    keyObject = new ArrayList<>();
                    keyPrimitive = userCursor.getBlob(i);
                    for(j = 0; j < keyPrimitive.length; j++)
                    {
                        keyObject.add(keyPrimitive[j]);
                    }
                    userFields.put(userCursor.getColumnName(i), keyObject);
                }
            }
        }

        userCursor.close();

        return userFields;
    }

    /**
     * This function fetches the list of chats the system user is a part of.
     * @return The system user's chats (ArrayList<HashMap<String, Object>>).
     */
    public ArrayList<HashMap<String, Object>> getChats()
    {
        Cursor chatCursor = sqliteDB.rawQuery(CHATS_TABLE_QUERY_ALL_CHAT_DATA_SQL, null);
        ArrayList<HashMap<String, Object>> allChats = new ArrayList<>();
        HashMap<String, Object> chatFields;
        int i;

        while (chatCursor.moveToNext())
        {
            chatFields = new HashMap<>();

            for (i = 0; i < chatCursor.getColumnCount(); i++) {
                if (chatCursor.getColumnName(i).equals(CHATS_TABLE_QUERY_USER_DATA_COLUMNS_SQL[1]))
                {
                    chatFields.put(chatCursor.getColumnName(i), chatCursor.getString(i));
                }
                else
                {
                    chatFields.put(chatCursor.getColumnName(i), chatCursor.getInt(i));
                }
            }

            if(!chatFields.isEmpty())
            {
                allChats.add(chatFields);
            }
        }

        chatCursor.close();

        return allChats;
    }

    /**
     * This function fetches the list of messages in a given chat.
     * @param chat_id The id of the chat (int).
     * @return All of the messages in the chat (ArrayList<HashMap<String, Object>>).
     */
    public ArrayList<HashMap<String, Object>> getMessages(int chat_id)
    {
        Cursor messageCursor = sqliteDB.rawQuery(MESSAGES_TABLE_QUERY_ALL_CHAT_DATA_SQL + chat_id + MESSAGES_TABLE_QUERY_ORDER_BY_TIME, null);
        ArrayList<HashMap<String, Object>> allMessages = new ArrayList<>();
        HashMap<String, Object> messageFields;
        int i;

        while (messageCursor.moveToNext())
        {
            messageFields = new HashMap<>();

            for (i = 0; i < messageCursor.getColumnCount(); i++) {
                if (messageCursor.getColumnName(i).equals(MESSAGES_TABLE_QUERY_USER_DATA_COLUMNS_SQL[1]))
                {
                    messageFields.put(messageCursor.getColumnName(i), messageCursor.getString(i));
                }
                else if (messageCursor.getColumnName(i).equals(MESSAGES_TABLE_QUERY_USER_DATA_COLUMNS_SQL[2]))
                {
                    messageFields.put(messageCursor.getColumnName(i), messageCursor.getLong(i));
                }
                else
                {
                    messageFields.put(messageCursor.getColumnName(i), messageCursor.getInt(i));
                }
            }

            if(!messageFields.isEmpty())
            {
                allMessages.add(messageFields);
            }
        }

        messageCursor.close();

        return allMessages;
    }
}
