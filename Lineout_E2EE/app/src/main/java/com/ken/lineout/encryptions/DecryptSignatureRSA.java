package com.ken.lineout.encryptions;

import com.ken.lineout.encryptions.lineout_key_generators.KeyGenerator;

import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;

public class DecryptSignatureRSA extends DecryptRSA{

    /**
     * Constructor for the RSA encoder class using a RSAPair object.
     * @param key The private key used for decryption (byte[]).
     */
    public DecryptSignatureRSA(byte[] key) {
        super(key);
    }

    /**
     * This function decodes a numerical value using RSA decryption.
     * @param key The public key used for decryption (byte[]).
     * @param value The value to be decrypted by the function (byte[]).
     * @return The decrypted value (byte[]).
     * @throws NoSuchAlgorithmException if hashing algorithm does not exist.
     */
    @Override
    public byte[] decrypt(byte[] key, byte[] value) throws NoSuchAlgorithmException
    {
        BigInteger bigValue = new BigInteger(1, value);
        BigInteger bigPublic = new BigInteger(key);
        BigInteger result;

        result = bigValue.modPow(KeyGenerator.PUBLIC_KEY_MODIFIER, bigPublic);
        return unpadOAEP(result);
    }

    /**
     * This function strips the OAEP padding from the encoded data.
     * @param paddedValue The OAEP encoded data (BigInteger).
     * @return The plain value (byte[]).
     * @throws NoSuchAlgorithmException if hashing algorithm does not exist.
     */
    @Override
    protected byte[] unpadOAEP(BigInteger paddedValue) throws NoSuchAlgorithmException
    {
        return super.unpadOAEP(paddedValue);
    }
}
