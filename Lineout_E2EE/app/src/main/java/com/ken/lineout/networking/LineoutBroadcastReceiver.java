package com.ken.lineout.networking;

import android.app.Notification;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.ken.lineout.MenuActivity;
import com.ken.lineout.R;
import com.ken.lineout.SignupActivity;
import com.ken.lineout.database.DatabaseWrapperSQL;
import com.ken.lineout.encryptions.CryptoManager;
import com.ken.lineout.encryptions.HashMaker;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.HashMap;

public class LineoutBroadcastReceiver extends BroadcastReceiver implements Serializable
{
    private static final String TAG = "LineoutReceiver";

    public static final String SIGNUP_ACTION = "com.lineout.action.SIGNUP";
    public static final String LOGIN_ACTION = "com.lineout.action.LOGIN";
    public static final String ADD_FRIEND_ACTION = "com.lineout.action.ADD_FRIEND";
    public static final String ADD_MESSAGE_ACTION = "com.lineout.action.ADD_MESSAGE";
    public static final String GET_MESSAGES_ACTION = "com.lineout.action.GET_MESSAGES";
    public static final String ERROR_ACTION = "com.lineout.action.ERROR";


    private final DatabaseWrapperSQL wrapperSQL;
    private CryptoManager cryptoManager;
    private String content = "";
    private String username = "";
    private byte[] publicKey = new byte[1];
    private byte[] privateKey = new byte[1];
    private long timeSent;
    private int senderId;
    private int chatId;

    /**
     * This function creates an instance of the LineoutBroadcastReceiver object.
     * @param wrapperSQL The Local Lineout Database Wrapper (DatabaseWrapperSQL).
     */
    public LineoutBroadcastReceiver(DatabaseWrapperSQL wrapperSQL)
    {
        this.wrapperSQL = wrapperSQL;
    }

    /**
     * This function creates an instance of the LineoutBroadcastReceiver object.
     * @param wrapperSQL The Local Lineout Database Wrapper (DatabaseWrapperSQL).
     * @param username The username that will be used for relevant writes/reads from the database (String).
     */
    public LineoutBroadcastReceiver(DatabaseWrapperSQL wrapperSQL, String username)
    {
        this.wrapperSQL = wrapperSQL;
        this.username = username;
    }

    /**
     * This function creates an instance of the LineoutBroadcastReceiver object.
     * @param wrapperSQL The Local Lineout Database Wrapper (DatabaseWrapperSQL).
     * @param cryptoManager The Lineout Crypto Manager (CryptoManager).
     * @param publicKey The RSA public key that will be used for relevant writes/reads from the database (byte[]).
     */
    public LineoutBroadcastReceiver(DatabaseWrapperSQL wrapperSQL, CryptoManager cryptoManager, byte[] publicKey)
    {
        this.wrapperSQL = wrapperSQL;
        this.cryptoManager = cryptoManager;
        this.publicKey = publicKey;
    }

    /**
     * This function creates an instance of the LineoutBroadcastReceiver object (Preferable for Signup).
     * @param wrapperSQL The Local Lineout Database Wrapper (DatabaseWrapperSQL).
     * @param username The username that will be used for relevant writes/reads from the database (String).
     * @param publicKey The RSA public key that will be used for relevant writes/reads from the database (byte[]).
     * @param privateKey The RSA private key that will be used for relevant writes/reads from the database (byte[]).
     */
    public LineoutBroadcastReceiver(DatabaseWrapperSQL wrapperSQL, String username, byte[] publicKey, byte[] privateKey)
    {
        this.wrapperSQL = wrapperSQL;
        this.username = username;
        this.publicKey = publicKey;
        this.privateKey = privateKey;
    }

    /**
     * This function creates an instance of the LineoutBroadcastReceiver object.
     * @param wrapperSQL The Local Lineout Database Wrapper (DatabaseWrapperSQL).
     * @param cryptoManager The Lineout Crypto Manager (CryptoManager).
     * @param senderId The id of the sender (int).
     * @param chatId The id of the chat (int).
     * @param timeSent The timestamp of the message (long).
     * @param content The contents of the message (String).
     */
    public LineoutBroadcastReceiver(DatabaseWrapperSQL wrapperSQL, CryptoManager cryptoManager, String content, long timeSent, int senderId, int chatId)
    {
        this.wrapperSQL = wrapperSQL;
        this.cryptoManager = cryptoManager;
        this.content = content;
        this.timeSent = timeSent;
        this.senderId = senderId;
        this.chatId = chatId;
    }

    /**
     * This function handles broadcasts sent to it from other activities.
     * @param context The context of the broadcast (Context).
     * @param intent The intent containing relevant information for the receiver to handle (Intent).
     */
    @Override
    public void onReceive(Context context, Intent intent) {
        final PendingResult pendingResult = goAsync();
        Task asyncTask = new Task(pendingResult, context, intent, wrapperSQL, cryptoManager, username, content, publicKey, privateKey, timeSent, senderId, chatId);
        asyncTask.execute();
    }

    private static class Task extends AsyncTask<String, Integer, String> {

        private final PendingResult pendingResult;
        private final Intent intent;
        private final Context context;

        private final NotificationManagerCompat notificationManager;

        private final DatabaseWrapperSQL wrapperSQL;
        private final CryptoManager cryptoManager;

        private final String username;
        private final String content;
        private final byte[] publicKey;
        private final byte[] privateKey;
        private final long timeSent;
        private final int senderId;
        private final int chatId;

        private Task(PendingResult pendingResult, Context context, Intent intent, DatabaseWrapperSQL wrapperSQL, CryptoManager cryptoManager, String username, String content, byte[] publicKey, byte[] privateKey, long timeSent, int senderId, int chatId) {
            this.pendingResult = pendingResult;
            this.context = context;
            this.intent = intent;
            this.wrapperSQL = wrapperSQL;
            this.cryptoManager = cryptoManager;
            this.username = username;
            this.content = content;
            this.publicKey = publicKey;
            this.privateKey = privateKey;
            this.timeSent = timeSent;
            this.senderId = senderId;
            this.chatId = chatId;
            this.notificationManager = NotificationManagerCompat.from(this.context);
        }

        /**
         * This function runs in the background and handles the broadcast.
         * @param strings ...
         * @return The action that was executed (String).
         */
        @Override
        protected String doInBackground(String... strings) {
            String action = intent.getAction();
            String notifContent;
            Notification notification;
            HashMap<String, Object> friendFields;
            boolean resultCode;
            int id = 0;

            switch (action) {
                case ERROR_ACTION:
                    notifContent = intent.getStringExtra("data");
                    notification = new NotificationCompat.Builder(context, LineoutApp.NOTIFICATION_CHANNEL_ID)
                            .setSmallIcon(R.drawable.ic_baseline_wifi_off_24)
                            .setContentTitle("Server Error")
                            .setContentText(notifContent)
                            .setPriority(NotificationCompat.PRIORITY_HIGH)
                            .setCategory(NotificationCompat.CATEGORY_MESSAGE)
                            .build();
                    notificationManager.notify(-1, notification);
                    break;

                case SIGNUP_ACTION:
                    resultCode = intent.getBooleanExtra("data", false);

                    if (resultCode && wrapperSQL.insertUser(username, publicKey, privateKey)) {
                        notifContent = "Signup Successful";
                    } else {
                        notifContent = "Username Taken";
                    }

                    notification = new NotificationCompat.Builder(context, LineoutApp.NOTIFICATION_CHANNEL_ID)
                            .setSmallIcon(R.drawable.ic_baseline_wifi_off_24)
                            .setContentTitle("Signup Request")
                            .setContentText(notifContent)
                            .setPriority(NotificationCompat.PRIORITY_HIGH)
                            .setCategory(NotificationCompat.CATEGORY_MESSAGE)
                            .build();
                    notificationManager.notify(1, notification);
                    break;

                case LOGIN_ACTION:
                    resultCode = intent.getBooleanExtra("data", false);

                    if (resultCode) {
                        notifContent = "Login Successful";
                    } else {
                        notifContent = "Username/Password are Wrong";
                    }

                    notification = new NotificationCompat.Builder(context, LineoutApp.NOTIFICATION_CHANNEL_ID)
                            .setSmallIcon(R.drawable.ic_baseline_wifi_off_24)
                            .setContentTitle("Login Request")
                            .setContentText(notifContent)
                            .setPriority(NotificationCompat.PRIORITY_HIGH)
                            .setCategory(NotificationCompat.CATEGORY_MESSAGE)
                            .build();
                    notificationManager.notify(1, notification);

                    if (resultCode) {
                        Intent intent = new Intent(context, MenuActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra(SignupActivity.CRYPT_NAME, cryptoManager);
                        intent.putExtra(SignupActivity.KEY_NAME, publicKey);
                        context.startActivity(intent);
                    }
                    break;

                case ADD_FRIEND_ACTION:
                    byte[] friendKey = intent.getByteArrayExtra("data");

                    if(friendKey.length <= 1)
                    {
                        break;
                    }

                    if(!wrapperSQL.getFriendData(username).isEmpty())
                    {
                        break;
                    }

                    if (wrapperSQL.insertFriend(username, friendKey)) {
                        friendFields = wrapperSQL.getFriendData(username);
                        id = (Integer) friendFields.get("ID");

                        if (wrapperSQL.insertChat(username, id)) {
                            notifContent = "Added " + username + " to Friend List";
                        } else {
                            notifContent = "Account Does Not Exist";
                        }
                    } else {
                        notifContent = "Couldn't Add " + username + " due to Database Error";
                    }

                    notification = new NotificationCompat.Builder(context, LineoutApp.NOTIFICATION_CHANNEL_ID)
                            .setSmallIcon(R.drawable.ic_baseline_wifi_off_24)
                            .setContentTitle("Add Friend Request")
                            .setContentText(notifContent)
                            .setPriority(NotificationCompat.PRIORITY_HIGH)
                            .setCategory(NotificationCompat.CATEGORY_MESSAGE)
                            .build();
                    notificationManager.notify(2, notification);
                    break;

                case ADD_MESSAGE_ACTION:
                    resultCode = intent.getBooleanExtra("data", false);

                    if (resultCode && wrapperSQL.insertMessage(content, timeSent, chatId, senderId)) {
                        notifContent = "Message Sent Successfully";
                    } else {
                        notifContent = "Failed to Send Message";
                    }

                    notification = new NotificationCompat.Builder(context, LineoutApp.NOTIFICATION_CHANNEL_ID)
                            .setSmallIcon(R.drawable.ic_baseline_wifi_off_24)
                            .setContentTitle("Messages")
                            .setContentText(notifContent)
                            .setPriority(NotificationCompat.PRIORITY_HIGH)
                            .setCategory(NotificationCompat.CATEGORY_MESSAGE)
                            .build();
                    notificationManager.notify(3, notification);
                    break;

                case GET_MESSAGES_ACTION:
                    ArrayList<String> messages = (ArrayList<String>) intent.getExtras().get("data");
                    ArrayList<HashMap<String, Object>> chats;
                    ArrayList<HashMap<String, Object>> existingMessages;
                    ArrayList<Byte> givenKeyAsObject, existingKeyAsObject;
                    JSONObject msg;
                    JSONArray givenKeyAsJson;
                    HashMaker mask;
                    String content, sender, signatureStr;
                    byte[] givenKeyAsPrimitive, existingKeyAsPrimitive, hashedContent, encryptedContent, signature;
                    boolean flag = true, repost = false;
                    long timestamp;
                    int chatID;

                    try {
                        mask = new HashMaker(MessageDigest.getInstance("SHA-256"));
                        for (String strMsg : messages) {
                            msg = new JSONObject(strMsg);
                            sender = msg.getString("sender");
                            content = msg.getString("content");
                            Log.d(TAG, "doInBackground: " + content);
                            timestamp = msg.getLong("timestamp");
                            givenKeyAsJson = msg.getJSONArray("pub_key");

                            givenKeyAsObject = new ArrayList<>();
                            for(int i = 0; i < givenKeyAsJson.length(); i++)
                            {
                                givenKeyAsObject.add(((Integer) givenKeyAsJson.getInt(i)).byteValue());
                            }

                            givenKeyAsPrimitive = new byte[givenKeyAsObject.size()];
                            for (int i = 0; i < givenKeyAsObject.size(); i++) {
                                givenKeyAsPrimitive[i] = givenKeyAsObject.get(i);
                            }

                            friendFields = wrapperSQL.getFriendData(sender);
                            if(friendFields.isEmpty())
                            {
                                wrapperSQL.insertFriend(sender, givenKeyAsPrimitive);
                                friendFields = wrapperSQL.getFriendData(sender);
                                id = (Integer) friendFields.get("ID");
                                wrapperSQL.insertChat(sender, id);
                            }

                            existingKeyAsObject = (ArrayList<Byte>) friendFields.get("PUBLIC_KEY");
                            existingKeyAsPrimitive = new byte[existingKeyAsObject.size()];
                            for (int i = 0; i < existingKeyAsObject.size(); i++) {
                                existingKeyAsPrimitive[i] = existingKeyAsObject.get(i);
                            }

                            if(Arrays.equals(givenKeyAsPrimitive, existingKeyAsPrimitive)) {
                                signatureStr = msg.getString("signature");
                                if (signatureStr.length() != 1)
                                {
                                    encryptedContent = Base64.getDecoder().decode(signatureStr.getBytes());
                                    signature = cryptoManager.decryptData(CryptoManager.RSA_SIG_MODE, encryptedContent, existingKeyAsPrimitive);
                                    hashedContent = mask.generateMask(content.getBytes(), MessageDigest.getInstance("SHA-256").getDigestLength());
                                    for (int i = 0; i < signature.length && i < hashedContent.length; i++) {
                                        if (hashedContent[i] != signature[i]) {
                                            flag = false;
                                            break;
                                        }
                                    }
                                }
                                else
                                {
                                    flag = false;
                                }
                            }
                            else
                            {
                                flag = false;
                            }

                            if (flag) {
                                if(id == 0) {
                                    id = (Integer) friendFields.get("ID");
                                }
                                chats = wrapperSQL.getChats();

                                for (HashMap<String, Object> chat : chats) {
                                    if ((Integer) chat.get("MEM1_ID") == id) {
                                        chatID = (Integer) chat.get("CHAT_ID");

                                        if (wrapperSQL.countMessages() != 0) {
                                            existingMessages = wrapperSQL.getMessages(chatID);
                                            for (HashMap<String, Object> existingMsg : existingMessages) {
                                                if ((Long) existingMsg.get("TIME_SENT") == timestamp && (Integer) existingMsg.get("SENDER_ID") == id && (Integer) existingMsg.get("CHAT_ID") == chatID) {
                                                    repost = true;
                                                    break;
                                                }
                                            }
                                        }

                                        if (!repost) {
                                            if (wrapperSQL.insertMessage(content, timestamp, chatID, id)) {
                                                notification = new NotificationCompat.Builder(context, LineoutApp.NOTIFICATION_CHANNEL_ID)
                                                        .setSmallIcon(R.drawable.ic_baseline_wifi_off_24)
                                                        .setContentTitle(sender)
                                                        .setContentText(content)
                                                        .setPriority(NotificationCompat.PRIORITY_HIGH)
                                                        .setCategory(NotificationCompat.CATEGORY_MESSAGE)
                                                        .build();
                                                notificationManager.notify(4, notification);
                                            }
                                        } else {
                                            repost = false;
                                        }

                                        break;
                                    }
                                }
                            }
                            else
                            {
                                flag = true;
                            }
                        }
                    } catch (Exception e ) {
                        e.printStackTrace();
                    }
                    break;

                default:
                    Log.d(TAG, "doInBackground: Unknown Action");
                    break;
            }

            return action;
        }

        /**
         * This function runs once the current task has completed its job.
         * @param s ...
         */
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            // Must call finish() so the BroadcastReceiver can be recycled.
            pendingResult.finish();
        }
    }

}
