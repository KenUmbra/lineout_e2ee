package com.ken.lineout;

import android.content.Intent;
import android.content.IntentFilter;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.ken.lineout.database.DatabaseWrapperSQL;
import com.ken.lineout.encryptions.CryptoManager;
import com.ken.lineout.networking.LineoutApp;
import com.ken.lineout.networking.LineoutBroadcastReceiver;
import com.ken.lineout.networking.LineoutWriter;
import com.ken.lineout.packet_handling.PacketSerializer;

import java.io.IOException;
import java.net.Socket;
import java.util.HashMap;

public class AddFriendActivity extends AppCompatActivity {

    private CryptoManager cryptoManager;
    private PacketSerializer serializer;

    private DatabaseWrapperSQL wrapperSQL;

    private LineoutApp lineoutApp;

    private IntentFilter intentFilter = new IntentFilter();
    private LineoutBroadcastReceiver broadcastReceiver = null;
    private LineoutWriter lineoutWriter;
    private Thread writerThread;
    private Socket connectionSocket;

    private byte[] publicKey;

    private EditText usernameET;
    private Button addFriendBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_friend);
        Intent retIntent = getIntent();
        SQLiteDatabase db = openOrCreateDatabase(DatabaseWrapperSQL.DB_FILE_NAME, MODE_PRIVATE, null);
        lineoutApp = (LineoutApp) getApplication();
        wrapperSQL = new DatabaseWrapperSQL(db);

        intentFilter.addAction(LineoutBroadcastReceiver.ERROR_ACTION);
        intentFilter.addAction(LineoutBroadcastReceiver.ADD_FRIEND_ACTION);

        publicKey = retIntent.getByteArrayExtra(SignupActivity.KEY_NAME);
        cryptoManager = (CryptoManager) retIntent.getSerializableExtra(SignupActivity.CRYPT_NAME);
        serializer = new PacketSerializer(cryptoManager);

        connectionSocket = lineoutApp.getMasterSocket();

        usernameET = findViewById(R.id.friend_username_et);
        addFriendBtn = findViewById(R.id.add_friend_button);

        addFriendBtn.setOnClickListener(this::onClickAddFriend);
    }

    /**
     * This function handles clicks on the add friend button.
     * @param view The current view (View).
     */
    public void onClickAddFriend(View view)
    {
        PacketSerializer.Packet addFriendPacket = new PacketSerializer.Packet();

        if (!usernameET.getText().toString().equals("") && SignupActivity.USERNAME_MAX_LENGTH > usernameET.getText().toString().length()) {
            addFriendPacket.opcode = PacketSerializer.ADD_FRIEND_OPCODE;

            addFriendPacket.fields = new HashMap<>();
            addFriendPacket.fields.put("dest_username", usernameET.getText().toString());

            broadcastReceiver = new LineoutBroadcastReceiver(wrapperSQL, usernameET.getText().toString());
            lineoutApp.setLineoutBroadcastReceiver(broadcastReceiver, intentFilter);

            try {
                Log.d("TAG", "onClickAddFriend: " + (connectionSocket == null));
                lineoutWriter = new LineoutWriter(connectionSocket.getOutputStream(), serializer, addFriendPacket);
            } catch (IOException e) {
                e.printStackTrace();
            }

            writerThread = new Thread(lineoutWriter);
            writerThread.start();
        }
        else
        {
            Toast.makeText(getApplicationContext(), "Friend's Name Must Between 1 to 32 Characters Long", Toast.LENGTH_SHORT).show();
        }

    }
}