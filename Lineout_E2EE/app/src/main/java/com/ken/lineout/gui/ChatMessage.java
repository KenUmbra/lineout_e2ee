package com.ken.lineout.gui;

public class ChatMessage {
    public boolean left;
    public String message;

    /**
     * This function creates a new instance of a ChatMessage Object.
     * @param left True - the message was sent by a friend, False - The Message was sent by the user (boolean).
     * @param message The content of the message (String).
     */
    public ChatMessage(boolean left, String message) {
        super();
        this.left = left;
        this.message = message;
    }
}
