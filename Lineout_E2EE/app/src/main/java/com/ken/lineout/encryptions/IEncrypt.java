package com.ken.lineout.encryptions;

public interface IEncrypt
{
    default byte[] encrypt(byte[] key, byte[] value) throws Exception {
        return new byte[0];
    }
}