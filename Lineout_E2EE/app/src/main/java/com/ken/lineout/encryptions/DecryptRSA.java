package com.ken.lineout.encryptions;

import android.util.Log;

import java.io.Serializable;
import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import com.ken.lineout.encryptions.lineout_key_generators.*;

public class DecryptRSA implements IDecrypt, Serializable
{
    private static final int R_SEED_LENGTH = 64;
    private static final int PADDING_GAP = KeyGenerator.RSA_KEY_SIZE_BITS - (R_SEED_LENGTH * 8) - 128;
    private final byte[] privateKey;

    /**
     * Constructor for the RSA encoder class using a RSAPair object.
     * @param key The private key used for decryption (byte[]).
     */
    public DecryptRSA(byte[] key)
    {
        this.privateKey = key;
    }

    /**
     * This function decodes a numerical value using RSA decryption.
     * @param key The public key used for decryption (byte[]).
     * @param value The value to be decrypted by the function (byte[]).
     * @return The decrypted value (byte[]).
     * @throws NoSuchAlgorithmException if hashing algorithm does not exist.
     */
    public byte[] decrypt(byte[] key, byte[] value) throws NoSuchAlgorithmException
    {
        BigInteger bigValue = new BigInteger(1, value);
        BigInteger bigPrivate = new BigInteger(privateKey);
        BigInteger bigPublic = new BigInteger(key);
        BigInteger result;

        result = bigValue.modPow(bigPrivate, bigPublic);
        if(result.toByteArray().length > 114)
        {
            return new byte[1];
        }
        return unpadOAEP(result);
    }

    /**
     * This function strips the OAEP padding from the encoded data.
     * @param paddedValue The OAEP encoded data (BigInteger).
     * @return The plain value (byte[]).
     * @throws NoSuchAlgorithmException if hashing algorithm does not exist.
     */
    protected byte[] unpadOAEP(BigInteger paddedValue) throws NoSuchAlgorithmException
    {
        HashMaker maskG = new HashMaker(MessageDigest.getInstance("SHA-1"));
        HashMaker maskH = new HashMaker(MessageDigest.getInstance("SHA-256"));
        ByteBuffer byteBuffer = ByteBuffer.wrap(paddedValue.toByteArray());
        BigInteger leftSide;
        BigInteger rightSide;
        BigInteger paddingNeeded;
        BigInteger decodedData;
        BigInteger decodedRSeed;
        byte[] encodedData;
        byte[] encodedRString;
        byte[] paddingNeededBytes;
        byte[] mgf1;
        byte[] mgf2;
        byte[] finalBuffer;
        int index = 0;

        encodedData = new byte[PADDING_GAP / 8];
        byteBuffer.get(encodedData, 0, encodedData.length);
        index += encodedData.length;

        encodedRString = new byte[R_SEED_LENGTH];
        byteBuffer.get(encodedRString, 0, encodedRString.length);
        index += encodedRString.length;

        paddingNeededBytes = new byte[paddedValue.toByteArray().length - index];
        byteBuffer.get(paddingNeededBytes, 0, paddingNeededBytes.length);
        paddingNeeded = new BigInteger(paddingNeededBytes);

        mgf2 = maskH.generateMask(encodedData, R_SEED_LENGTH);
        leftSide = new BigInteger(encodedRString);
        rightSide = new BigInteger(mgf2);
        decodedRSeed = leftSide.xor(rightSide);

        mgf1 = maskG.generateMask(decodedRSeed.toByteArray(), PADDING_GAP / 8);
        leftSide = new BigInteger(encodedData);
        rightSide = new BigInteger(mgf1);
        decodedData = leftSide.xor(rightSide);

        finalBuffer = new byte[decodedData.toByteArray().length - paddingNeeded.intValue()];
        System.arraycopy(decodedData.toByteArray(), 0, finalBuffer, 0, finalBuffer.length);

        return finalBuffer;
    }
}
