package com.ken.lineout.encryptions;

import java.io.Serializable;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Random;

import com.ken.lineout.encryptions.lineout_key_generators.*;

public class EncryptRSA implements IEncrypt, Serializable
{
    private static final int R_SEED_LENGTH = 64;
    private static final int PADDING_GAP = KeyGenerator.RSA_KEY_SIZE_BITS - (R_SEED_LENGTH * 8) - 128;    

    /**
     * This function encrypts a numerical value using RSA encryption.
     * @param key The public RSA key used for encryption (byte[]).
     * @param value The numerical value to be encrypted by the function (byte[]).
     * @return The encrypted value (byte[]).
     * @throws NoSuchAlgorithmException if hashing algorithm does not exist.
     */
    public byte[] encrypt(byte[] key, byte[] value) throws NoSuchAlgorithmException
    {
        byte[] encryptedValue;
        BigInteger paddedValue;
        BigInteger bigKey = new BigInteger(1, key);

        do {
            paddedValue = padOAEP(value);
        } while(paddedValue.signum() == -1);
        
        encryptedValue = (paddedValue.modPow(KeyGenerator.PUBLIC_KEY_MODIFIER, bigKey)).toByteArray();

        return encryptedValue;
    }

    /**
     * This function generates padding for the plain data using the OAEP padding scheme for improved security.
     * @param plainValue The value to pad (byte[]).
     * @return The padded value (BigInteger).
     * @throws NoSuchAlgorithmException if hashing algorithm does not exist.
     */
    protected BigInteger padOAEP(byte[] plainValue) throws NoSuchAlgorithmException
    {
        HashMaker maskG = new HashMaker(MessageDigest.getInstance("SHA-1"));
        HashMaker maskH = new HashMaker(MessageDigest.getInstance("SHA-256"));

        String rSeed = this.generateRandomString(R_SEED_LENGTH);
        BigInteger bigValue = new BigInteger(plainValue);
        BigInteger paddingNeeded = BigInteger.valueOf(((PADDING_GAP - bigValue.bitLength()) / 8));
        BigInteger leftSide;
        BigInteger rightSide;
        BigInteger encodedData;
        BigInteger encodedRString;
        byte[] paddedData;
        byte[] finalBuffer;
        byte[] mgf1;
        byte[] mgf2;
        int index = 0;

        paddingNeeded = paddingNeeded.subtract(BigInteger.valueOf(paddingNeeded.bitLength() / 8));
        paddedData = new byte[plainValue.length + paddingNeeded.intValue()];
        System.arraycopy(plainValue, 0, paddedData, 0, plainValue.length);

        mgf1 = maskG.generateMask(rSeed.getBytes(), PADDING_GAP / 8);
        leftSide = new BigInteger(paddedData);
        rightSide = new BigInteger(mgf1);
        encodedData = leftSide.xor(rightSide);

        mgf2 = maskH.generateMask(encodedData.toByteArray(), R_SEED_LENGTH);
        leftSide = new BigInteger(rSeed.getBytes());
        rightSide = new BigInteger(mgf2);
        encodedRString = leftSide.xor(rightSide);

        finalBuffer = new byte[encodedData.toByteArray().length + encodedRString.toByteArray().length + paddingNeeded.toByteArray().length];
        System.arraycopy(encodedData.toByteArray(), 0, finalBuffer, index, encodedData.toByteArray().length);
        index += encodedData.toByteArray().length;
        System.arraycopy(encodedRString.toByteArray(), 0, finalBuffer, index, encodedRString.toByteArray().length);
        index += encodedRString.toByteArray().length;
        System.arraycopy(paddingNeeded.toByteArray(), 0, finalBuffer, index, paddingNeeded.toByteArray().length);

        return new BigInteger(finalBuffer);
    }

    /**
     * This function generates a random, alphanumeric string at the intended length.
     * @param length The desired length of the string (int).
     * @return The newly generated string (String).
     */
    private String generateRandomString(int length) 
    {
        final String UPPERCASE = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

        String symbols = UPPERCASE + UPPERCASE.toLowerCase() + "0123456789";
        Random random = new SecureRandom();
        char[] randomBuffer = new char[length];

        for (int idx = 0; idx < randomBuffer.length; ++idx)
        {
            randomBuffer[idx] = symbols.charAt(random.nextInt(symbols.length()));
        }
        return new String(randomBuffer);
    }
}
