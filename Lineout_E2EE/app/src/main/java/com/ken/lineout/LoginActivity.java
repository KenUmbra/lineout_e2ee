package com.ken.lineout;

import android.content.Intent;
import android.content.IntentFilter;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.ken.lineout.database.DatabaseWrapperSQL;
import com.ken.lineout.encryptions.CryptoManager;
import com.ken.lineout.networking.LineoutApp;
import com.ken.lineout.networking.LineoutBroadcastReceiver;
import com.ken.lineout.networking.LineoutWriter;
import com.ken.lineout.packet_handling.PacketSerializer;

import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;

public class LoginActivity extends AppCompatActivity {

    private CryptoManager cryptoManager;
    private PacketSerializer serializer;

    private DatabaseWrapperSQL wrapperSQL;

    private IntentFilter intentFilter = new IntentFilter();
    private LineoutBroadcastReceiver broadcastReceiver = null;
    private LineoutWriter lineoutWriter;
    private Thread writerThread;
    private Socket connectionSocket;

    private byte[] publicKey;

    private EditText usernameET, passwordET;
    private Button loginBtn, moveToSignupBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        LineoutApp lineoutApp = (LineoutApp) getApplication();
        Intent retIntent = getIntent();
        SQLiteDatabase db = openOrCreateDatabase(DatabaseWrapperSQL.DB_FILE_NAME, MODE_PRIVATE, null);
        wrapperSQL = new DatabaseWrapperSQL(db);

        intentFilter.addAction(LineoutBroadcastReceiver.ERROR_ACTION);
        intentFilter.addAction(LineoutBroadcastReceiver.LOGIN_ACTION);

        publicKey = retIntent.getByteArrayExtra(SignupActivity.KEY_NAME);
        cryptoManager = (CryptoManager) retIntent.getSerializableExtra(SignupActivity.CRYPT_NAME);
        serializer = new PacketSerializer(cryptoManager);

        connectionSocket = lineoutApp.getMasterSocket();

        broadcastReceiver = new LineoutBroadcastReceiver(wrapperSQL, cryptoManager, publicKey);
        lineoutApp.setLineoutBroadcastReceiver(broadcastReceiver, intentFilter);

        usernameET = findViewById(R.id.login_username_et);
        passwordET = findViewById(R.id.login_password_et);
        loginBtn = findViewById(R.id.login_button);
        moveToSignupBtn = findViewById(R.id.move_to_signup_page_button);

        loginBtn.setOnClickListener(this::onClickLogin);

        moveToSignupBtn.setOnClickListener(view -> {
            Intent intent = new Intent(LoginActivity.this, SignupActivity.class);
            intent.putExtra(SignupActivity.CRYPT_NAME, cryptoManager);
            intent.putExtra(SignupActivity.KEY_NAME, publicKey);
            startActivity(intent);
        });
    }

    /**
     * This function handles clicks on the login button.
     * @param view The current view (View).
     */
    public void onClickLogin(View view)
    {
        PacketSerializer.Packet loginPacket = new PacketSerializer.Packet();
        ArrayList<Byte> publicKeyRSA = new ArrayList<>();

        if(wrapperSQL.countFriends() != 0) {
            if (!usernameET.getText().toString().equals("") && !passwordET.getText().toString().equals("") && SignupActivity.USERNAME_MAX_LENGTH > usernameET.getText().toString().length()) {
                loginPacket.opcode = PacketSerializer.LOGIN_OPCODE;

                loginPacket.fields = new HashMap<>();
                loginPacket.fields.put("username", usernameET.getText().toString());
                loginPacket.fields.put("password", passwordET.getText().toString());
                for (byte b : publicKey) {
                    publicKeyRSA.add(b);
                }
                loginPacket.fields.put("pub_key", publicKeyRSA);

                try {
                    lineoutWriter = new LineoutWriter(connectionSocket.getOutputStream(), serializer, loginPacket);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                writerThread = new Thread(lineoutWriter);
                writerThread.start();
            }
        }
        else
        {
            Toast.makeText(getApplicationContext(), "No Lineout Account Exists Locally", Toast.LENGTH_SHORT).show();
        }
    }
}