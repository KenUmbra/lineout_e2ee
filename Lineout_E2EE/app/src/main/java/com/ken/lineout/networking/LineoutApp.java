package com.ken.lineout.networking;

import android.app.Application;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.IntentFilter;
import android.os.Build;

import androidx.annotation.RequiresApi;

import java.net.Socket;

public class LineoutApp extends Application {
    public static final String NOTIFICATION_CHANNEL_ID = "lineoutNotifications";
    private Socket masterSocket;
    private LineoutBroadcastReceiver lineoutBroadcastReceiver = null;

    @Override
    public void onCreate()
    {
        super.onCreate();
        NotificationChannel lineoutNotifs = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "lineoutNotifications", NotificationManager.IMPORTANCE_HIGH);
        lineoutNotifs.setDescription("Lineout Notification Channel");
        NotificationManager manager = getSystemService(NotificationManager.class);
        manager.createNotificationChannel(lineoutNotifs);
    }

    /**
     * This function returns the master socket of the Lineout application.
     * @return The server socket that connects to the server (Socket).
     */
    public Socket getMasterSocket() {
        return masterSocket;
    }

    /**
     * This function sets the master socket of the Lineout application.
     * @param masterSocket The new socket that connects to the database (Socket).
     */
    public void setMasterSocket(Socket masterSocket) {
        this.masterSocket = masterSocket;
    }

    /**
     * This function sets the receiver for the Lineout application, and removes the one currently used if needed.
     * @param lineoutBroadcastReceiver The new broadcast receiver to be used (LineoutBroadcastReceiver).
     * @param intentFilter The new intent filter to be used (IntentFilter).
     */
    public void setLineoutBroadcastReceiver(LineoutBroadcastReceiver lineoutBroadcastReceiver, IntentFilter intentFilter) {
        if(this.lineoutBroadcastReceiver != null)
        {
            this.unregisterReceiver(this.lineoutBroadcastReceiver);
        }

        this.lineoutBroadcastReceiver = lineoutBroadcastReceiver;
        this.registerReceiver(this.lineoutBroadcastReceiver, intentFilter);
    }
}
