package com.ken.lineout.networking;

import android.content.Context;
import android.util.Log;

import com.ken.lineout.packet_handling.PacketDeserializer;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.net.Socket;

public class LineoutManager extends Thread implements Serializable {
    private static final String TAG = "LineoutManager";
    private static final String SERVER_IP = "10.0.2.2";
    private static final int SERVER_PORT = 2960;

    private Socket socket;
    private InputStream inputStream;
    private OutputStream outputStream;
    private PacketDeserializer deserializer;
    private Context context;
    private LineoutReader readerThread;

    /**
     * This function creates an instance of a Lineout Manager object.
     * @param deserializer
     */
    public LineoutManager(PacketDeserializer deserializer, Context context)
    {
        this.deserializer = deserializer;
        this.context = context;
    }

    /**
     * This function connects to the server's socket.
     */
    @Override
    public void run() {
        try {
            socket = new Socket(SERVER_IP, SERVER_PORT);
            outputStream = socket.getOutputStream();
            inputStream = socket.getInputStream();
            Log.d(TAG, "run: Manager Running");

            readerThread = new LineoutReader(socket, inputStream, deserializer, context);
            readerThread.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * This function returns the output stream of the socket.
     * @return The socket's output stream (OutputStream).
     */
    public OutputStream getOutputStream() {
        return outputStream;
    }

    /**
     * This function returns the socket that is connected to the Lineout server.
     * @return The connection socket (Socket).
     */
    public Socket getSocket() {
        return socket;
    }
}
