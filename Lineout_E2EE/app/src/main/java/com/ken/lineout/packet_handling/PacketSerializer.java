package com.ken.lineout.packet_handling;

import android.util.Log;

import androidx.annotation.NonNull;

import com.ken.lineout.encryptions.CryptoManager;
import com.ken.lineout.encryptions.lineout_key_generators.KeyGenerator;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.Serializable;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class PacketSerializer implements Serializable {
    private static final String TAG = "PacketSerializer";

    public static final byte NO_OPCODE = -1;
    public static final byte SIGNUP_OPCODE = 1;
    public static final byte LOGIN_OPCODE = 3;
    public static final byte ADD_FRIEND_OPCODE = 11;
    public static final byte ADD_MESSAGE_OPCODE = 13;
    public static final byte GET_MESSAGES_OPCODE = 15;

    public static class Packet
    {
        public byte opcode;
        public Map<String, Object> fields;
    }

    private CryptoManager cryptoManager;

    /**
     * This function crates an instance of the PacketSerializer class.
     * @param cryptoManager The user's CryptoManager (CryptoManager).
     */
    public PacketSerializer(CryptoManager cryptoManager)
    {
        this.cryptoManager = cryptoManager;
    }

    /**
     * This function serializes a packet with one layer of encryption.
     * @param packet The packet to serialize (Packet).
     * @param destKeyRSA The destination's public RSA key (byte[]).
     * @return The serialized packet (byte[]).
     * @throws Exception if one of the encryption methods fails.
     */
    public byte[] serializePacket(@NonNull Packet packet, byte[] destKeyRSA) throws Exception
    {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        ByteBuffer shortBuffer = ByteBuffer.allocate(2);
        JSONObject jsonFields = new JSONObject(packet.fields);
        byte[] keyAES;
        byte[] encryptedKey, encryptedData, finalBuffer;

        do {
            keyAES = KeyGenerator.generateKeyAES();
        } while(keyAES.length != KeyGenerator.AES_KEY_SIZE_BYTES);

        do {
            encryptedKey = cryptoManager.encryptData(CryptoManager.RSA_MODE, keyAES, destKeyRSA);
        } while(encryptedKey.length != (KeyGenerator.RSA_KEY_SIZE_BITS / 8));

        encryptedData = cryptoManager.encryptData(CryptoManager.AES_MODE, jsonFields.toString().getBytes(), keyAES);

        Log.d(TAG, "serializePacket: " + encryptedData.length + "___" + encryptedKey.length);

        if(packet.opcode != NO_OPCODE) {
            outputStream.write(packet.opcode);
        }

        outputStream.write((byte) encryptedKey.length);
        outputStream.write(encryptedKey);

        shortBuffer.putShort((short) encryptedData.length);
        outputStream.write(shortBuffer.array());
        outputStream.write(encryptedData);

        finalBuffer = outputStream.toByteArray();

        return finalBuffer;
    }

    /**
     * This function serializes a packet with two layers of encryption.
     * @param packet The packet to serialize (Packet).
     * @param destKeyRSA The destination's public RSA key (byte[]).
     * @param destUsername The destination's username (String).
     * @param serverKeyRSA The server's public RSA key (byte[]).
     * @return The serialized packet (byte[]).
     * @throws Exception if one of the encryption methods fails.
     */
    public byte[] serializePacket(@NonNull Packet packet, byte[] destKeyRSA, String destUsername, byte[] serverKeyRSA) throws Exception
    {
        final String FIELD1 = "dest_username";
        final String FIELD2 = "message";

        ArrayList<Byte> objectBuffer = new ArrayList<>();
        Packet outerPacket = new Packet();
        byte[] serializedInnerPacket;

        outerPacket.opcode = packet.opcode;
        packet.opcode = NO_OPCODE;
        serializedInnerPacket = serializePacket(packet, destKeyRSA);

        for (byte b: serializedInnerPacket) {
            objectBuffer.add(b);
        }

        outerPacket.fields = new HashMap<>();
        outerPacket.fields.put(FIELD1, destUsername);
        outerPacket.fields.put(FIELD2, objectBuffer);

        Log.d(TAG, "serializePacket: " + outerPacket.fields);

        return serializePacket(outerPacket, serverKeyRSA);
    }
}
