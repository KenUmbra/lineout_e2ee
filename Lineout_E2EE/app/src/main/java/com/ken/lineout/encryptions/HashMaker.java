package com.ken.lineout.encryptions;

import java.io.Serializable;
import java.security.MessageDigest;

public class HashMaker implements Serializable
{
    private MessageDigest digest;
    
    /**
     * This function creates an instance of the HashMaker object with the wanted hash.
     * 
     * @param digest digest to use as the basis of the function (MessageDigest).
     */
    public HashMaker(MessageDigest digest)
    {
        this.digest = digest;
    }
    
    /***
     * This function converts a number into a byte array.
     * @param i The number to convert (int).
     * @param sp The target byte aeeay (byte[]).
     */
    private void int2ByteArray(int i,byte[] sp)
    {
        sp[0] = (byte)(i >>> 24);
        sp[1] = (byte)(i >>> 16);
        sp[2] = (byte)(i >>> 8);
        sp[3] = (byte)(i >>> 0);
    }
    
    /**
     * This function hashes an input & cuts it into the desired length.
     * @param seed The seed to hash (byte[]).
     * @param length The desired length of the hash (int).
     * @return The hashed input (byte[]).
     */
    public byte[] generateMask(byte[] seed, int length)
    {
        byte[] mask = new byte[length];
        byte[] C = new byte[4];
        int counter = 0;
        int hLen = digest.getDigestLength();

        digest.reset();

        while (counter < (length / hLen))
        {
            int2ByteArray(counter, C);

            digest.update(seed);
            digest.update(C);

            System.arraycopy(digest.digest(), 0, mask, counter * hLen, hLen);
            
            counter++;
        }

        if ((counter * hLen) < length)
        {
            int2ByteArray(counter, C);

            digest.update(seed);
            digest.update(C);

            System.arraycopy(digest.digest(), 0, mask, counter * hLen, mask.length - (counter * hLen));
        }

        return mask;
    }
}