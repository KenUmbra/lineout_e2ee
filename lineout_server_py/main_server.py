from base64 import b64encode, b64decode
from AESCipher import AESCipher
from RSACipher import RSACipher
from database_wrapper import database_wrapper
from threading import Lock
import hashlib
import socket
import json
import ast
import _thread

SERVER_PORT = 2960

CODE_RECV_SIZE = 1
KEY_SIZE_RECV_SIZE = 1
SIZE_RECV_SIZE = 2
DEFAULT_RECV_SIZE = 1024

AES_KEY_SIZE = 16

AES_MANAGER = AESCipher()
RSA_MANAGER = RSACipher()

RSA_PUBLIC = b'h1Um9vM3wOw+w/m3P+nADPFLc86erVdToZ7WZRe7uT66riYRIS0QdInW9ng1s9NjP/FOWxy5Jm/p5nPolo4Fj2/b/+qdgQZBaHbxp0hv1Qoy/DhJnheCBnDXYvJFgMTyz6pskcU5CBz8Wbr6u3Q8pIuVYab7IXVT+86e+8Kmf9k='
RSA_PRIVATE = b'Z9jIOzSDRkczHNBP/TQGqPdqKSqBKnL1bST33Fpi3HjPxBQsRgR0bsOEoQKu41v6cdW4lH2YbfQYdrh2mg/39gfliylSIxZcrHdKY9J3A7ybfK23nIUqP86bpVS79S73+iNenAbgsZWo22bADhhLsLTsZ84jomgyXyCbCXcSE+8='

db_mutex = Lock()

def serialize_packet(code, json_data, rsa_key) -> bytes:
    """_summary_
    This function serializes a packet.
    Args:
        code (bytes): The packets' opcode.
        json_data (str): The packet's JSON fields.
        rsa_key (bytes): The user's public RSA key.

    Returns:
        bytes: The encrypted packet with all of its fields.
    """
    key_aes = AES_MANAGER.generate_key()

    encrypted_key = RSA_MANAGER.encrypt(int.from_bytes(key_aes, byteorder='big', signed=True), int.from_bytes(rsa_key, 'big', signed=False))

    try:
        encrypted_key_bytes = encrypted_key.to_bytes(int(round(encrypted_key.bit_length() / 8)), 'big')
    except OverflowError:
        encrypted_key_bytes = encrypted_key.to_bytes(int(round(encrypted_key.bit_length() / 8)) + 1, 'big')
    encrypted_data = AES_MANAGER.encrypt(json_data, key_aes)
    
    final_buffer = code + len(encrypted_key_bytes).to_bytes(KEY_SIZE_RECV_SIZE, 'big') + encrypted_key_bytes + len(encrypted_data).to_bytes(SIZE_RECV_SIZE, 'big') + encrypted_data
    return final_buffer

def deserialize_packet(client_sock) -> dict:
    """_summary_
    This function deserializes a packet.
    Args:
        client_sock (socket): The user's socket.

    Returns:
        dict: The packet's JSON fields.
    """
    raw = client_sock.recv(KEY_SIZE_RECV_SIZE)
    key_length = int.from_bytes(raw, 'big', signed=False)
    encrypted_key = client_sock.recv(key_length)
    decrypted_key = RSA_MANAGER.decrypt(int.from_bytes(encrypted_key, 'big', signed=False), int.from_bytes(b64decode(RSA_PRIVATE), 'big', signed=False), int.from_bytes(b64decode(RSA_PUBLIC), 'big', signed=False))
    try:
        key_aes = decrypted_key.to_bytes(round(decrypted_key.bit_length() / 8), 'big')
    except OverflowError:
        key_aes = decrypted_key.to_bytes(round(decrypted_key.bit_length() / 8) + 1, 'big')
    
    if len(key_aes) < AES_KEY_SIZE:
        key_aes = key_aes = decrypted_key.to_bytes(AES_KEY_SIZE, 'big')
    
    raw = client_sock.recv(SIZE_RECV_SIZE)
    data_length = int.from_bytes(raw, 'big', signed=False)
    encrypted_data = client_sock.recv(data_length)
    plain_data = AES_MANAGER.decrypt(encrypted_data, key_aes)
    
    return json.loads(plain_data.rstrip('\x00'))

def generate_error(error_message) -> bytes:
    """_summary_
    This function generates an error message.
    Args:
        error_message (str): The error message string.

    Returns:
        bytes: The encoded error message.
    """
    code = 0
    code = code.to_bytes(CODE_RECV_SIZE, 'big')
    res_dict = {"reason_for_error": error_message}
    return code + json.dumps(res_dict).encode()

def signup(client_sock, current_user):
    """_summary_
    This function signs up an unknown user to Lineout.
    Args:
        client_sock (socket): The user's socket.
        current_user (str): The current user's username.

    Returns:
        (int, str): The current user's mode & The current user's username.
    """
    data = deserialize_packet(client_sock)
    username = data["username"]
    password = data["password"]
    rsa_key = data["pub_key"]
    
    for i in range(len(rsa_key)):
        if rsa_key[i] < 0:
            rsa_key[i] += 256
    
    rsa_key = bytes(rsa_key)
    
    sql_db = database_wrapper()
    db_mutex.acquire()
    try:
        code = 2
        code = code.to_bytes(CODE_RECV_SIZE, 'big')
        
        res_dict = {"result": -1}
        if sql_db.insert_user(username, password, rsa_key):
            res_dict["result"] = 1
            print("Another User Signed Up Successfully")
        else:
           res_dict["result"] = 0
           print("Another User Couldn't Sign Up")
    finally:
        db_mutex.release()
    
    result_packet = serialize_packet(code, json.dumps(res_dict), rsa_key)
    client_sock.sendall(result_packet)
    return 0, current_user
    
def login(client_sock, current_user):
    """_summary_
    This function logs in an existing user to Lineout.
    Args:
        client_sock (socket): The user's socket.
        current_user (str): The current user's username.

    Returns:
        (int, str): The current user's mode & The current user's username.
    """
    data = deserialize_packet(client_sock)
    username = data["username"]
    password = data["password"]
    rsa_key = data["pub_key"]
    
    for i in range(len(rsa_key)):
        if rsa_key[i] < 0:
            rsa_key[i] += 256
    
    rsa_key = bytes(rsa_key)
    
    sql_db = database_wrapper()
    db_mutex.acquire()
    try:
        code = 4
        code = code.to_bytes(CODE_RECV_SIZE, 'big')
        
        res_dict = {"result": -1}
        if sql_db.are_credentials_correct(username, password):
            res_dict["result"] = 1
            rsa_key = sql_db.get_user_key(username)
            current_user = username
            print("Another User Logged In Successfully")
        else:
           res_dict["result"] = 0
           print("Another User Couldn't Log In")
    finally:
        db_mutex.release()
    
    result_packet = serialize_packet(code, json.dumps(res_dict), rsa_key)
    client_sock.sendall(result_packet)
    return res_dict["result"], current_user

def add_friend(client_sock, current_user):
    """_summary_
    This function adds a friend to the user's Lineout account.
    Args:
        client_sock (socket): The user's socket.
        current_user (str): The current user's username.

    Returns:
        (int, str): The current user's mode & The current user's username.
    """
    data = deserialize_packet(client_sock)
    dest_username = data["dest_username"]
    
    sql_db = database_wrapper()
    db_mutex.acquire()
    
    try:
        code = 12
        code = code.to_bytes(CODE_RECV_SIZE, 'big')
        rsa_key = sql_db.get_user_key(current_user)
        
        res_dict = {"key": []}
        if dest_username != current_user:
            friend_key = list(sql_db.insert_friend(current_user, dest_username))
            if 1 < len(friend_key):
                res_dict["key"] = friend_key
                print("Another Pair of Users are Friends")
            else:
                print("Another Pair of Users aren't Friends")
                
    finally:
        db_mutex.release()
    
    result_packet = serialize_packet(code, json.dumps(res_dict), rsa_key)
    client_sock.sendall(result_packet)
    return 1, current_user

def add_message(client_sock, current_user):
    """_summary_
    This function adds a message destined to one of the user's friends.
    Args:
        client_sock (socket): The user's socket.
        current_user (str): The current user's username.

    Returns:
        (int, str): The current user's mode & The current user's username.
    """
    data = deserialize_packet(client_sock)
    dest_username = data["dest_username"]
    encrypted_msg = data["message"]
    
    for i in range(len(encrypted_msg)):
        if encrypted_msg[i] < 0:
            encrypted_msg[i] += 256
    
    encrypted_msg = bytes(encrypted_msg)
    
    sql_db = database_wrapper()
    db_mutex.acquire()
    
    try:
        code = 14
        code = code.to_bytes(CODE_RECV_SIZE, 'big')
        rsa_key = sql_db.get_user_key(current_user)
        
        res_dict = {"result": -1}
        if sql_db.insert_msg(current_user, dest_username, encrypted_msg):
            res_dict["result"] = 1
            print("Another User sent a Message")
        else:
            res_dict["result"] = 0
            print("Another User failed to send a Message")
    finally:
        db_mutex.release()
    
    result_packet = serialize_packet(code, json.dumps(res_dict), rsa_key)
    client_sock.sendall(result_packet)
    return 1, current_user

def fetch_messages(client_sock, current_user):
    """_summary_
    This function fetches all of the messages intended for the current user.
    Args:
        client_sock (socket): The user's socket.
        current_user (str): The current user's username.

    Returns:
        (int, str): The current user's mode & The current user's username.
    """
    sql_db = database_wrapper()
    db_mutex.acquire()
    
    try:
        code = 16
        code = code.to_bytes(CODE_RECV_SIZE, 'big')
        rsa_key = sql_db.get_user_key(current_user)
        
        res_dict = {"messages": []}
        msg_array = sql_db.get_user_messages(current_user)
        
        res_dict["messages"] = msg_array
        print("Another User got his Messages")
    finally:
        db_mutex.release()
    
    result_packet = serialize_packet(code, json.dumps(res_dict), rsa_key)
    client_sock.sendall(result_packet)
    return 1, current_user

LOGGED_OUT_ACTIONS_DICT = {
    1: signup,
    3: login,
    -1: generate_error
}

LOGGED_IN_ACTIONS_DICT = {
    3: login,
    11: add_friend,
    13: add_message,
    15: fetch_messages,
    -1: generate_error
}

USER_MODES_DICT = {
    0: LOGGED_OUT_ACTIONS_DICT,
    1: LOGGED_IN_ACTIONS_DICT
}

def on_new_client(client_sock, client_addr):
    """_summary_
    This function handles communication between the server and the newly connected client.
    Args:
        client_sock (socket): The socket that can interact with the respective client.
        client_addr (str): The client's IP address.
    """
    user_mode = 0
    current_user = "guest"
    
    try:
        with client_sock:
            while True:
                code = client_sock.recv(CODE_RECV_SIZE)
                code = int.from_bytes(code, 'big', signed=False)
                try:
                    if code in USER_MODES_DICT[user_mode]:
                        user_mode, current_user = USER_MODES_DICT[user_mode][code](client_sock, current_user)
                    else:
                        client_sock.sendall(USER_MODES_DICT[user_mode][-1]("Unknown Code"))
                except KeyError:
                    pass
    except ConnectionResetError:
        pass
    except ConnectionAbortedError:
        pass
    except BrokenPipeError:
        pass

def communicator(listening_sock):
    """_summary_
    This function handles the initial communication between the server and the clients. 
    Args:
        listening_sock (socket): The socket acting as the server's listening socket.
    """
    while True:
        try:
            client_sock, client_addr = listening_sock.accept()
            print("New user connected: " + client_addr[0])
            _thread.start_new_thread(on_new_client, (client_sock, client_addr))
        except ConnectionResetError:
            pass
        except ConnectionAbortedError:
            pass
        except KeyboardInterrupt:
            break


def main():
    with socket.socket() as listening_socket:
        server_addr = ('', SERVER_PORT)
        listening_socket.bind(server_addr)
        listening_socket.listen(6)
        communicator(listening_socket)
        
        
if __name__ == '__main__':
    main()