import socket
import json

SERVER_IP = "127.0.0.1"
SERVER_PORT = 2960

CODE_RECV_SIZE = 1
SIZE_RECV_SIZE = 2
DEFAULT_RECV_SIZE = 1024


def signup():
    code = 1
    code = code.to_bytes(CODE_RECV_SIZE, 'big')
    username = input("Enter username: ")
    password = input("Enter password: ")
    signup_dict = {"username": username, "password": password}
    signup_packet = code + json.dumps(signup_dict).encode()
    sock.sendall(signup_packet)
    print(sock.recv(DEFAULT_RECV_SIZE).decode())

def login():
    code = 3
    code = code.to_bytes(CODE_RECV_SIZE, 'big')
    username = input("Enter username: ")
    password = input("Enter password: ")
    login_dict = {"username": username, "password": password}
    login_packet = code + json.dumps(login_dict).encode()
    sock.sendall(login_packet)
    print(sock.recv(DEFAULT_RECV_SIZE).decode())

def main():
    with socket.socket() as sock:
        try:
            server_addr = (SERVER_IP, SERVER_PORT)
            sock.connect(server_addr)
        except ConnectionRefusedError:
            print("crashed")
        except ConnectionResetError:
            print("crashed")
        except KeyboardInterrupt:
            pass
        
    
    
if __name__ == '__main__':
    main()