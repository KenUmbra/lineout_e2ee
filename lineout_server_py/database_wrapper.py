import sqlite3
import random
import string
import hashlib

ACCOUNTS_TABLE_CREATION = '''CREATE TABLE IF NOT EXISTS ACCOUNTS(ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, 
                                USERNAME VARCHAR(32) NOT NULL, PASSWORD VARCHAR(32) NOT NULL, SALT VARCHAR(32) NOT NULL, RSA_KEY BINARY NOT NULL)'''
MESSAGES_TABLE_CREATION = '''CREATE TABLE IF NOT EXISTS MESSAGES(ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
                                RECIEVER_ID INTEGER NOT NULL, ENCRYPTED_MSG BINARY NOT NULL)'''
FRIENDS_TABLE_CREATION = '''CREATE TABLE IF NOT EXISTS FRIENDS(ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, 
                                FRIEND_1_ID INTEGER NOT NULL, FRIEND_2_ID INTEGER NOT NULL)'''

INSERT_USER = "INSERT INTO ACCOUNTS(USERNAME, PASSWORD, SALT, RSA_KEY) VALUES(?, ?, ?, ?)"
INSERT_MSG = "INSERT INTO MESSAGES(RECIEVER_ID, ENCRYPTED_MSG) VALUES(?, ?)"
INSERT_FRIEND = "INSERT INTO FRIENDS(FRIEND_1_ID, FRIEND_2_ID) VALUES(?, ?)"

FIND_USER_BY_NAME_QUERY = "SELECT * FROM ACCOUNTS WHERE USERNAME=:name"
FIND_USER_BY_ID_QUERY = "SELECT * FROM ACCOUNTS WHERE ID=:id"
FIND_FRIEND_QUERY = "SELECT * FROM FRIENDS WHERE FRIEND_1_ID=:id OR FRIEND_2_ID=:id"
FETCH_MESSAGES_FOR_USER_QUERY = "SELECT (ENCRYPTED_MSG) FROM MESSAGES WHERE RECIEVER_ID=:id"

SALT_LENGTH = 32


class database_wrapper(object):
    def __init__(self) -> None:
        """_summary_
        This function initalizes the database_wrapper object.
        """
        self.db_con = sqlite3.connect('lineoutDatabase.db')
        self.db_cursor = self.db_con.cursor()
        self.db_cursor.execute(ACCOUNTS_TABLE_CREATION)
        self.db_cursor.execute(MESSAGES_TABLE_CREATION)
        self.db_cursor.execute(FRIENDS_TABLE_CREATION)
    
    def __del__(self):
        """_summary_
        This function deletes the database_wrapper object.
        """
        self.db_con.commit()
        self.db_con.close()
    
    def insert_user(self, username, password, rsa_key) -> bool:
        """_summary_
        This function inserts a new user into the database.
        Args:
            username (str): The user's name.
            password (str): The user's password.
            rsa_key (bytes): The user's public RSA key.

        Returns:
            bool: True - Action Successful, False - Action Failed.
        """
        if self.get_user_id(username) == -1:
            salt = str(''.join(random.choices(string.ascii_uppercase + string.digits, k = SALT_LENGTH)))
            hashed_password = hashlib.sha256((password + salt).encode()).digest()
            self.db_cursor.execute(INSERT_USER, (username, hashed_password, salt, rsa_key))
            return True
        return False
    
    def insert_msg(self, sender, reciever, encrypted_msg) -> bool:
        """_summary_
        This function inserts a new message into the database.
        Args:
            sender (str): The sender's username.
            reciever (str): The reciever's username.
            encrypted_msg (bytes): The encrypted message intended for the reciever.

        Returns:
            bool: True - Action Successful, False - Action Failed.
        """
        sender_id = self.get_user_id(sender)
        reciever_id = self.get_user_id(reciever)
        if sender_id != reciever_id != -1 and self.__are_friends(sender_id, reciever_id):
            self.db_cursor.execute(INSERT_MSG, (reciever_id, encrypted_msg))
            return True
        return False
    
    def insert_friend(self, friend1, friend2) -> bytes:
        """_summary_
        This function inserts a new pair of friends into the database.
        Args:
            friend1 (str): Friend 1's username.
            friend2 (str): Friend 2's username.

        Returns:
            bytes: The friend's public RSA key.
        """
        friend1_id = self.get_user_id(friend1)
        friend2_id = self.get_user_id(friend2)
       
       
        if friend1_id != friend2_id != -1 and not self.__are_friends(friend1_id, friend2_id):
            self.db_cursor.execute(INSERT_FRIEND, (friend1_id, friend2_id))
            for row in self.db_cursor.execute(FIND_USER_BY_NAME_QUERY, {"name": friend2}):
                return row[4]
        elif self.__are_friends(friend1_id, friend2_id):
            for row in self.db_cursor.execute(FIND_USER_BY_NAME_QUERY, {"name": friend2}):
                return row[4]
        return b'0'
    
    def get_user_messages(self, username) -> list:
        """_summary_
        This function fetches messages intended for a user.
        Args:
            username (str): The username of the user.

        Returns:
            list: A list containing all of the encrypted messages intended for the user.
        """
        msg_list = []
        user_id = self.get_user_id(username)

        if user_id != -1:
            for row in self.db_cursor.execute(FETCH_MESSAGES_FOR_USER_QUERY, {"id": user_id}):
                msg_list.append(list(row[0]))
        return msg_list

    def get_user_id(self, username) -> int:
        """_summary_
        This function fetches the user's id based on their username.
        Args:
            username (str): The user's username.

        Returns:
            int: The id of the user.
        """
        if len(username) <= 32:
            for row in self.db_cursor.execute(FIND_USER_BY_NAME_QUERY, {"name": username}):
                return row[0]
        return -1
    
    def get_username(self, user_id) -> str:
        """_summary_
        This function fetches the user's name based on their id.
        Args:
            user_id (int): The id of the user.

        Returns:
            str: The username of the user.
        """
        for row in self.db_cursor.execute(FIND_USER_BY_ID_QUERY, {"id": user_id}):
            return row[1]
        return ""

    def get_user_key(self, username) -> bytes:
        """_summary_
        This function fetches the user's public RSA key.
        Args:
            username (str): The user's username.

        Returns:
            bytes: The user's public RSA key.
        """
        if len(username) <= 32:
            for row in self.db_cursor.execute(FIND_USER_BY_NAME_QUERY, {"name": username}):
                return row[4]
        return b''

    def are_credentials_correct(self, username, password) -> bool:
        """_summary_
        This function checks if the user's credentials match the ones in the database.
        Args:
            username (str): The username of the user that is trying to login.
            password (str): The password of the user that is trying to login.

        Returns:
            bool: True - The credentials match, False - The credentials don't match.
        """
        for row in self.db_cursor.execute(FIND_USER_BY_NAME_QUERY, {"name": username}):
            real_password = row[2]
            salt = row[3]
            hashed_password = hashlib.sha256((password + salt).encode()).digest()
            return real_password == hashed_password
        return False
            
    
    def __are_friends(self, friend1_id, friend2_id) -> bool:
        """_summary_
        This function checks whether two users are friends.
        Args:
            friend1_id (int): The id of friend 1.
            friend2_id (int): The id of friend 2.

        Returns:
            bool: True - The two users are friends, False - The two users are not friends.
        """
        if friend1_id != friend2_id != -1:
            for row in self.db_cursor.execute(FIND_FRIEND_QUERY, {"id": friend1_id}):
                if row[1] == friend2_id or row[2] == friend2_id:
                    return True        
        return False
