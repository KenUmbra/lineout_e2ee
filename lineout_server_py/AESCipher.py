import hashlib
import random
from Crypto import Random
from Crypto.Cipher import AES
from base64 import b64encode, b64decode

class AESCipher(object):
    def __init__(self):
        """_summary_
        This function initializes the AESCipher object.
        """
        self.block_size = AES.block_size

    def generate_key(self):
        """_summary_
        This function generates an AES-128 key.
        Returns:
            bytearray : The newly generated AES-128 key.
        """
        while True:
            key = random.sample(range(0, 255), 16)
            key = bytearray(key)
            if int.from_bytes(key, byteorder='big', signed=True) > 0:
                return key

    def encrypt(self, plain_text, key):
        """_summary_
        This function encrypts data using AES-128. 
        Args:
            plain_text (str): The data to be encrypted. 
            key (bytearray): The AES-128 key to be used.

        Returns:
            bytes : The encrypted data in a byte array.
        """
        plain_text = self.__pad(plain_text)
        cipher = AES.new(key, AES.MODE_ECB)
        encrypted_text = cipher.encrypt(plain_text.encode())
        return encrypted_text

    def decrypt(self, encrypted_text, key):
        """_summary_
        This function decrypts data using AES-128.
        Args:
            encrypted_text (bytes): The encrypted data in a byte array.
            key (bytearray): The AES-128 key to be used.

        Returns:
            str : The decrypted data.
        """
        cipher = AES.new(key, AES.MODE_ECB)
        plain_text = cipher.decrypt(encrypted_text).decode("utf-8")
        return plain_text

    def __pad(self, plain_text):
        """_summary_
        This function pads the data in case it does not fit into the 16 byte blocks.
        Args:
            plain_text (str): The data to be padded.

        Returns:
            str : The padded data.
        """
        number_of_bytes_to_pad = self.block_size - len(plain_text) % self.block_size
        ascii_string = chr(0)
        padding_str = number_of_bytes_to_pad * ascii_string
        padded_plain_text = plain_text + padding_str
        return padded_plain_text