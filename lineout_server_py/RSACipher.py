from Crypto.Util import number
import math
import hashlib
import string    
import random

class RSACipher(object):
    def __init__(self):
        """_summary_
        This function initializes the RSACipher object.
        """
        self.e = 4079
        self.key_size_in_bits = 1024
        self.r_seed_len = 64
        self.padding_gap = self.key_size_in_bits - (self.r_seed_len * 8) - 128
    
    def generate_keys(self):
        """_summary_
        This function generates a pair of RSA-1024 keys.
        Returns:
            (int, int) : The newly generated public key & the newly generated private key.
            
        """
        while True:
            p = number.getPrime(int(self.key_size_in_bits / 2))
            q = number.getPrime(int(self.key_size_in_bits / 2))
            mod = (p-1) * (q-1)
            if (math.gcd(mod, self.e) == 1) and ((p * q).bit_length() == self.key_size_in_bits):
                break
        public_key = p * q
        private_key = pow(self.e, -1, mod)
        
        return (public_key, private_key)
    
    def encrypt(self, plain_data, public_key):
        """_summary_
        This function encrypts an int using RSA-1024.
        Args:
            plain_data (int): The number to be encrypted.
            public_key (int): The RSA-1024 public key to be used during the encryption.

        Returns:
            int : The encrypted number.
        """
        while True:
            oaep_padded_number, signed_version = self.__pad_oaep(plain_data)
            if signed_version > 0:
                break
        return pow(oaep_padded_number, self.e, public_key)
    
    def decrypt(self, encrypted_data, private_key, public_key):
        """_summary_
        This function decrypts an int using RSA-1024.
        Args:
            encrypted_data (int): The number to be decrypted.
            private_key (int): The RSA-1024 private key to be used during the decryption.
            public_key (int): The RSA-1024 public key to be used during the decryption.

        Returns:
            int : The decrypted number.
        """
        oaep_padded_number = pow(encrypted_data, private_key, public_key)
        oaep_padded_number_len = int(round(oaep_padded_number.bit_length() / 8))
        if oaep_padded_number_len < 1:
            oaep_padded_number_len = 1
        try:
            oaep_padded_number = oaep_padded_number.to_bytes(oaep_padded_number_len, 'big')
        except OverflowError:
            oaep_padded_number = oaep_padded_number.to_bytes(oaep_padded_number_len + 1, 'big')
        return self.__unpad_oaep(oaep_padded_number)
    
    def __pad_oaep(self, plain_data):
        """_summary_
        This function generates padding for the plain data using the OAEP padding scheme for improved security.
        Args:
            plain_data (int): The data to add padding to.

        Returns:
            (int, int) : The encoded data (unsigned), the encoded data (signed).
        """
        padding_needed = int((self.padding_gap - plain_data.bit_length()) / 8)
        padding_needed -= int(round(padding_needed.bit_length() / 8))
        padding_needed_bytes_len = int(round(padding_needed.bit_length() / 8))
        if padding_needed_bytes_len < 1:
            padding_needed_bytes_len = 1
        padding_needed_bytes = padding_needed.to_bytes(padding_needed_bytes_len, 'big')
                
        byte_len = int(round(plain_data.bit_length() / 8))
        
        while True:
            try:
                byte_data = plain_data.to_bytes(byte_len, 'big')
                break
            except OverflowError:
                byte_len += 1
        
        byte_data += chr(0).encode() * padding_needed
        
        while True:
            r_string = str(''.join(random.choices(string.ascii_uppercase + string.digits, k = self.r_seed_len)))
            mgf1 = self.__hash_mask_function(r_string.encode(), int(self.padding_gap / 8))
            
            encoded_msg = int.from_bytes(byte_data, 'big', signed=False) ^ int.from_bytes(mgf1, 'big', signed=False)
            encoded_msg_len = int(round(encoded_msg.bit_length() / 8))
            
            while True:
                try:
                    encoded_msg = encoded_msg.to_bytes(encoded_msg_len, 'big')
                    break
                except OverflowError:
                    encoded_msg_len += 1
        
            mgf2 = self.__hash_mask_function(encoded_msg, self.r_seed_len, hash_func=hashlib.sha256)
            
            encoded_r_string = int.from_bytes(r_string.encode(), 'big', signed=False) ^ int.from_bytes(mgf2, 'big', signed=False)
            encoded_r_string_len = int(round(encoded_r_string.bit_length() / 8))
            
            while True:
                try:
                    encoded_r_string = encoded_r_string.to_bytes(encoded_r_string_len, 'big')
                    break
                except OverflowError:
                    encoded_r_string_len += 1
                
            if len(encoded_msg) == (self.padding_gap / 8) and len(encoded_r_string) == self.r_seed_len:
                break
        
        final_buffer = encoded_msg + encoded_r_string + padding_needed_bytes
        return int.from_bytes(final_buffer, 'big', signed=False), int.from_bytes(final_buffer, 'big', signed=True)
    
    def __unpad_oaep(self, encoded_data):
        """_summary_
        This function strips the OAEP padding from the encoded data.
        Args:
            encoded_data (bytes): The OAEP encoded data.

        Returns:
            int : The plain data.
        """
        encoded_msg = encoded_data[:(self.padding_gap // 8)]
        encoded_r_string = encoded_data[(self.padding_gap // 8):((self.padding_gap // 8) + self.r_seed_len)]
        padding_len = encoded_data[((self.padding_gap // 8) + self.r_seed_len):]
        padding_len = int.from_bytes(padding_len, 'big', signed=False)
        
        mgf2 = self.__hash_mask_function(encoded_msg, self.r_seed_len, hash_func=hashlib.sha256)
        
        r_string = int.from_bytes(encoded_r_string, 'big', signed=False) ^ int.from_bytes(mgf2, 'big', signed=False)
        r_string_len = int(round(r_string.bit_length() / 8))
        
        if r_string_len < 1:
            r_string_len = 1
        
        while True:
            try:
                r_string = r_string.to_bytes(r_string_len, 'big')
                break
            except OverflowError:
                r_string_len += 1
                
        r_string = str(r_string)[2:-1]
        
        mgf1 = self.__hash_mask_function(r_string.encode(), int(self.padding_gap / 8))
        
        msg = int.from_bytes(encoded_msg, 'big', signed=False) ^ int.from_bytes(mgf1, 'big', signed=False)
        msg_len = int(round(msg.bit_length() / 8))
        if msg_len < 1:
            msg_len = 1
        msg = msg.to_bytes(msg_len + 1, 'big')
        msg = msg[:-padding_len]
        return int.from_bytes(msg, 'big', signed=False)
        
    def __int_to_bytearray(self, integer, size=4):
        """_summary_
        This function converts a number into a byte array.
        Args:
            integer (int): The number to convert.
            size (int, optional): The amount of bytes in the number. Defaults to 4.

        Returns:
            bytes : The int in the form of a byte array.
        """
        return b"".join([chr((integer >> (8 * i)) & 0xFF).encode() for i in reversed(range(size))])

    def __hash_mask_function(self, input_seed, length, hash_func=hashlib.sha1):
        """_summary_
        This function hashes an input & cuts it into the desired length.
        Args:
            input_seed (bytes): The seed to hash.
            length (int): The desired length.
            hash_func (function, optional): The hash function to be used. Defaults to hashlib.sha1.

        Returns:
            bytes : The hashed input.
        """
        counter = 0
        output = b""
        while len(output) < length:
            C = self.__int_to_bytearray(counter, 4)
            output += hash_func(input_seed + C).digest()
            counter += 1
        return output[:length]
        
        