# Lineout_E2EE


## Name
Lineout - E2EE Edition

## Description
Lineout is a messenger app for Android devices I'm making as a school project.
As part of this project I am taking a deep dive into algorithms, with encryption algorithms being the primary focus.

As of now, I implemented my own versions of:
- RSA-1024 (Java/Python): Assymetrical Encryption Algorithm.
- RSA-OAEP (Java/Python): RSA Padding Scheme.
- AES-128 (Java): Symmetrical Encryption Algorithm.
- Python Server (Python): The (more or less) complete Lineout Server.
- Android Client (Java): The (more or less) complete Lineout Client.
- Minimal Text To Speech (Java): Chat Messages can be read using TTS.


## Badges
--

## Visuals
--

## Installation
--

## Usage
Lineout can be used as a messenger app by all, or as a way to learn about how to implement some famous encryption algorithms.

## Support
--

## Roadmap
Next Features to be implemented:
- N/A

Features in progress:
- N/A

Dropped Features:
- Python Client

Bonus Features:
- Implementation of Signal Protocol.

## Contributing
--

## Authors and acknowledgment
I have made most of this project on my own, however, my friend Harel helped me implement the basic form of RSA (as in, RSA without OAEP).

## License
GPL3.

## Project status
Completed.
